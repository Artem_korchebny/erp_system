<?

/* Данная функция, это большая часть кода с страницы salary.php, оптимизированный и доработанный для вывода выплат
для каждого сотрудника за весь период */

function getSal()
{		
	$db = getConnect();// подключаемся к БД
	$query1 = "SELECT month(date) FROM income ORDER BY date ASC";//извлекаем все записи из БД за нужный период
	$sql1 = mysqli_query($db, $query1);
	while ($monthDate = mysqli_fetch_assoc($sql1))
	{
		$proverka = $monthDate['month(date)'];//переменная для того что бы проверить пусто ли в БД
		$month2[] = $monthDate['month(date)'];
	}
	
	$month1 = array_values(array_unique($month2));//Убираем повторяющиеся месяца и обнуляем ключ массива для корректного вывода значений

	$queryEmployees = "SELECT * FROM employees";//извлекаем все записи из БД
	$sqlEmployees = mysqli_query($db, $queryEmployees);
	while ($rowPos = mysqli_fetch_assoc($sqlEmployees))
	{
		$employees[] = $rowPos;
	}
	
	$querySales_plan = "SELECT consultant, month(month), sum  FROM sales_plan";//извлекаем записи из БД
	$sqlSales_plan = mysqli_query($db, $querySales_plan);
	while ($rowSales_plan = mysqli_fetch_assoc($sqlSales_plan))
	{
		$Sales_plan[] = $rowSales_plan;
	}
	
	$queryIncome = "SELECT order_schet, sum, month(date) FROM income";//извлекаем записи из БД
	$sqlIncome = mysqli_query($db, $queryIncome);
	while ($rowIncome = mysqli_fetch_assoc($sqlIncome))
	{
		$Income[] = $rowIncome;
	}
	
	$queryOrders = "SELECT * FROM orders";//извлекаем все записи из БД
	$sqlOrders = mysqli_query($db, $queryOrders);
	while ($rowOrders = mysqli_fetch_assoc($sqlOrders))
	{
		$Orders[] = $rowOrders;
	}
	
	$querySpending = "SELECT * FROM spending";//извлекаем все записи из БД
	$sqlSpending = mysqli_query($db, $querySpending);
	while ($rowSpending = mysqli_fetch_assoc($sqlSpending))
	{
		$Spending[] = $rowSpending;
	}
	
	$queryProducts = "SELECT * FROM products";//извлекаем все записи из БД
	$sqlProducts = mysqli_query($db, $queryProducts);
	while ($rowProducts = mysqli_fetch_assoc($sqlProducts))
	{
		$Products[] = $rowProducts;
	}
	
	$queryWork = "SELECT * FROM complited_work";//извлекаем все записи из БД
	$sqlWork = mysqli_query($db, $queryWork);
	while ($rowWork = mysqli_fetch_assoc($sqlWork))
	{
		$work1[] =  $rowWork;
	}
	
	for ($wq = 0; $wq < count($month1); $wq++)//перебираем все месяца
	{
		$month = $month1[$wq];
		for ($lq = 0; $lq < count($Income); $lq++)//перебираем все данные массива $Income
		{
			if ($Income[$lq]["month(date)"] == $month)
			{
				$Income1[$month][] = $Income[$lq];// помещаем в массив $Income1, вложенные массивы, с данными по месяцам (ключ = номер месяца)
			}
		}
	}
	
	for ($rf = 0; $rf < count($month1); $rf++)//перебираем все месяца
	{
		$month = $month1[$rf];
		for ($dc = 0; $dc < count($Income1[$month]); $dc++)// перебираем все данные в массиве $Income1 за определенный месяц
		{
			$sumRow = $Income1[$month][$dc]["order_schet"];// помещаем в переменную номер счета
			for ($xz = 0; $xz < count($Orders); $xz++)//перебираем все данные массива $Orders
			{
				if ($Orders[$xz]["num"] == $sumRow)// если номера счетов совпадают, то
				{
					$Orders1[$month][] = $Orders[$xz];// помещаем в массив $Orders1 вложенные массивы, с данными по месяцам (ключ = номер месяца)
				}
			}
		}				
	}
	
	if (!empty($proverka))// Если в БД есть записи, то выводим шапку таблицы
	{
		echo "<h1>Остатки по ЗП</h1>";
		echo "<table id='myTable1'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>Сотрудник</th>";
		echo "<th>К выплате</th>";
		echo "</tr>";
		echo "</thead>";
	}

	for ($ee = 0; $ee < count($employees); $ee++)//перебираем всех сотрудников, и ниже подставляем каждого в код для расчета данных
	{
		//Обнуляем следующие массивы, чтобы при последующих итерациях были корректные значения
		$sumSal = array();
		$sumSal1= array();
		$ex = array();
		$ya = array();
		$paymants = array();
		$suma_viplat = array();
		$arSum = array();
		$arOr = array();
		$arOr1 = array();
		$mas = array();
		$allsumma = array();
		$allsummapercent = array();
		$premSum = array();
		$paymants1 = array();
		$work = array();
		$timeSum = array();
		$timeCharged = array();
		$n1 = array();
		$sumobshay1 = array();
		$cons1 = $employees[$ee]["name"];
		
		//считаем выплаты сотруднику
		for ($mn = 0; $mn < count($Spending); $mn++)//перебираем все данные массива $Spending
		{
			if ($Spending[$mn]["recipient"] == $cons1 AND $Spending[$mn]["item"] == "Зарплата")// если совпадает определенный сотрудник и статья = "Зарплата"
			{
				$paymants[] = $Spending[$mn]["sum"];// то помещаем в массив $paymants, сумма выплат
			}
		}
		$suma_viplat1 = @array_sum($paymants);//считаем общую сумму выплат
		
		$oklad = $employees[$ee]["salary"];// это оклад сотрудника
		
		if (!empty($oklad) || $cons1 == "Эдуард Вартанян" || $cons1 == "Георгий Слизинтуев")
		{
			for ($qq = 0; $qq < count($month1); $qq++)//перебираем все месяца
			{
				//Обнуляем следующие массивы, чтобы при последующих итерациях были корректные значения
				$month = $month1[$qq];
				$sumSal = array();
				$sumSal1= array();
				$ex = array();
				$ya = array();
				$paymants = array();
				$suma_viplat = array();
				$arSum = array();
				$arOr = array();
				$arOr1 = array();
				$mas = array();
				$allsumma = array();
				$allsummapercent = array();
				$premSum = array();
				$paymants1 = array();
				$work = array();
				$timeSum = array();
				$timeCharged = array();

				//получаем план продаж
				for ($zz = 0; $zz < count($Sales_plan); $zz++)//перебираем все данные массива $Sales_plan
				{
					if ($Sales_plan[$zz]["consultant"] == $cons1 AND $Sales_plan[$zz]["month(month)"] == $month)
					{
						$sumSales = $Sales_plan[$zz]["sum"];// план продаж
						break;
					}
					else
					{
						$sumSales = null;
					}	
				}
				
				// считаем сумму оплат по определенному сотруднику, и сумму всех оплат (необходимо для вычисления ЗП для Эдуарда Вартаняна и Георгия Слизинтуева)
				for ($bq = 0; $bq < count($Orders1[$month]); $bq++)
				{
					$sumSal1[] = $Orders1[$month][$bq]['sum'];// массив где все суммы всех оплат
					if ($Orders1[$month][$bq]["consultant"] == $cons1)
					{
						$sumSal[] = $Orders1[$month][$bq]['sum'];// массив где суммы всех оплат по определенному сотруднику
					}
				}

				$obSumSal = @array_sum($sumSal);//считаем общую сумму оплат по определенному сотруднику
				$obSumSal1 = @array_sum($sumSal1);//считаем общую сумму всех оплат

				
		//--------------------------------Сейчас будет расчет для сотрудников у которых есть оклад, но не ставка(почасовая оплата)---------------------------------------------------------------------------------

						
				for ($bw = 0; $bw < count($Orders1[$month]); $bw++)// перебираем все данные за определенный месяц из массива $Orders1
				{
					if ($Orders1[$month][$bw]["consultant"] == $cons1)// если консультант = определенному сотруднику, то добавляем данные в массивы
					{
						$arOr[] = $Orders1[$month][$bw];
						$arOr1[] = $Orders1[$month][$bw]['num'];
					}
					elseif ($Orders1[$month][$bw]["assistant"] == $cons1)// если ассистент = определенному сотруднику, то добавляем данные в массивы
					{
						$arOr[] = $Orders1[$month][$bw];
						$arOr1[] = $Orders1[$month][$bw]['num'];
					}
					elseif ($Orders1[$month][$bw]["manager"] == $cons1 AND $Orders1[$month][$bw]["assistant"] != $cons1 AND $Orders1[$month][$bw]["consultant"] != $cons1)// если менеджер = определенному сотруднику, и ассистент и консультант не равны определенному сотруднику то добавляем данные в массивы
					{
						$arOr[] = $Orders1[$month][$bw];
						$arOr1[] = $Orders1[$month][$bw]['num'];
					}
				}
			
				if (!empty($proverka))//если date в массиве не пустая
				{
					$mas = array();//создаём массив в который будет занасится сумма, консультан, продукт за определённый счёт
					for ($i = 0; $i < count($Income1[$month]); $i ++)//перебираем массив с данными из таблицы income
					{
						$sr = $Income1[$month][$i]['order_schet'];// помещаем в переменную номер счета из таблицы income
						$sumP = 0;
						for($j = 0; $j <count($arOr); $j ++)//перебираем массив с данными из таблицы orders
						{
							$or = $arOr[$j]['num'];// помещаем в переменную номер счета из таблицы orders
							if($sr == $or)// если номера счетов совпадают, то извлекаем информацию из orders и помещаем в переменные
							{
								$c = $arOr[$j]['consultant'];
								$a = $arOr[$j]['assistant'];
								$men = $arOr[$j]['manager'];
								$p = $arOr[$j]['product'];
								$percent = $arOr[$j]['assistant_percent'];
								$s = $arOr[$j]['sum'];
								$e = $arOr[$j]['expense'];
								$yan = $arOr[$j]['yandex'];
								// $e, $yan прибавляем для того чтобы, в таблице выводились записи (по условию запись выводится при равной сумме из income и orders)
								$sumP = $sumP + (int)$s + (int)$e + (int)$yan;
							}
						}

						if($sumP != 0)// если сумма не равна 0
						{
							$mas[$sr] = array("$sumP", $c, $p, $a, $men);//поместили в массив сумму, консультанта, продукт в котором ключь это номер счёта из таблицы income
						}

						$sr = $Income1[$month][$i]['order_schet'];// помещаем в переменную номер счета из таблицы orders
						foreach ($mas as $key => $value)//перебираем массив с данными из таблицы $mas
						{
							if ($sr == $key)//если номера счетов совпадают
							{
								$t =  $Income1[$month][$i]['sum'];
								if ($t == $value[0])//если суммы совпадают
								{
									for ($p = 0; $p < count($arOr); $p++)//перебираем массив с данными из таблицы $arOr
									{
										//если номера счетов совпадают и сумма не равна 0
										if (($sr == $arOr[$p]['num']) AND ($arOr[$p]['sum'] != 0))
										{
//Далее должны выводиться данные в таблицу(как на странице salary.php, но здесь нам это не нужно, поэтому опускаем этот код)

											$keyOrder = $key;
											$valueProduct = $arOr[$p]['num'];
											if ($keyOrder == $valueProduct)// если номера счетов совпадают
											{
												//условие определяющее выбор имени продукта
												if($arOr[$p]['product'] == null)
												{
													$productOrder = $arOr[$p]['appointment'];
												}
												else
												{
													$productOrder = $arOr[$p]['product'];
												}
											}

											$valueSumm = $arOr[$p]['num'];
											if ($keyOrder == $valueSumm)// если номера счетов совпадают
											{
												$summaOrder = $arOr[$p]['sum'];// помещаем в переменную сумму
												$allsumma[] = $arOr[$p]['sum'];// добавляем в массив сумму, для того чтобы потом посчитать итоговую сумму
											}

											//если консультант и менеджер равны определенному сотруднику, то считается процент
											if ($cons1 == $arOr[$p]['consultant'] && $cons1 == $arOr[$p]['manager'])
											{
												$percent1 = $percent/100;//ассистенский процент
												
												for($cx = 0; $cx <count($Products); $cx++)//перебираем массив с данными из таблицы orders
												{
													if ($Products[$cx]["product"] == $productOrder)
													{
														$z = $Products[$cx]["agency_percent"]/100;//агентский процент от продукта
														$man = $Products[$cx]["manager_percent"]/100;//менеджерский процент
														$obz = $Products[$cx]["agency_percent"]/100;//агентский процент от продукта
														$z = $z - $percent1 + $man; // процент если у него есть ассистент и менеджеh
														break;
													}
													else
													{
														$obz = 0.1;//агентский процент если продукт не найден
														$man = 0;//менеджерский процент
														$z = 0.1 - $percent1 + $man;
													}
												}
												
												$allsummapercent[] = $summaOrder*$z;// добавляем процент в массив, для того чтобы потом посчитать итоговый процент
													
												//если он выполнил план
												if ($sumSales != null)// Если план продаж есть, то считаем получает консультант премию или нет
												{
													if(($obSumSal > $sumSales) && ($obSumSal < ($sumSales + 50000)) && (!empty($sumSales)))
													{
														$pSum = $summaOrder * (0.15 - $obz);
														$premSum[] = $pSum;
													}
													//если он выполнил больше план, чем плана продаж + 50000
													elseif($obSumSal > ($sumSales + 50000) && (!empty($sumSales)))
													{
														$pSum = $summaOrder * (0.2 - $obz);
														$premSum[] = $pSum;
													}
												}
											}

											//если ассистент и менеджер равны определенному сотруднику, то счетается процент
											elseif ($cons1 == $arOr[$p]['assistant'] && $cons1 == $arOr[$p]['manager'])
											{
												$percent1 = $percent/100;
												
												for($cx = 0; $cx <count($Products); $cx ++)//перебираем массив с данными из таблицы orders
												{
													if ($Products[$cx]["product"] == $productOrder)
													{
														$z = $Products[$cx]['agency_percent']/100;//агентский процент от продукта
														$man = $Products[$cx]['manager_percent']/100;//менеджерский процент
														$z = $percent1 + $man;
														break;
													}
													else // если продукт не совпадает с продуктом из таблицы Products
													{
														$man = 0;
														$z = $percent1 + $man;
													}
												}
												
												$allsummapercent[] = $summaOrder*$z;
											}

											//если консультант равен определенному сотруднику, то считается процент
											elseif ($cons1 == $arOr[$p]['consultant'])
											{
												$percent1 = $percent/100;
												
												for($cx = 0; $cx <count($Products); $cx ++)//перебираем массив с данными из таблицы orders
												{
													if ($Products[$cx]["product"] == $productOrder)
													{
														$z = $Products[$cx]['agency_percent']/100;//агентский процент от продукта
														$obz = $Products[$cx]['agency_percent']/100;//агентский процент от продукта
														$z = $z - $percent1;
														break;
													}
													else // если продукт не совпадает с продуктом из таблицы Products
													{
														$obz = 0.1;
														$z = 0.1 - $percent1;
													}
												}
												
												$allsummapercent[] = $summaOrder*$z;// добавляем процент в массив, для того чтобы потом посчитать итоговый процент
												
												if ($sumSales != null)// Если план продаж есть, то считаем получает консультант премию или нет
												{
													if(($obSumSal > $sumSales) && ($obSumSal < ($sumSales + 50000)) && (!empty($sumSales)))
													{
														$pSum = $summaOrder * (0.15 - $obz);
														$premSum[] = $pSum;
													}
													elseif($obSumSal > ($sumSales + 50000) && (!empty($sumSales)))
													{
														$pSum = $summaOrder * (0.2 - $obz);
														$premSum[] = $pSum;
													}
												}
											}

											//если ассистент равен определенному сотруднику, то считается процент
											elseif ($cons1 == $arOr[$p]['assistant'])
											{
												$percent1 = $percent/100;
												for($cx = 0; $cx <count($Products); $cx ++)//перебираем массив с данными из таблицы orders
												{
													if ($Products[$cx]["product"] == $productOrder)
													{
														$z = $Products[$cx]['agency_percent']/100;
														$z = $percent1;
														break;																	
													}
													else // если продукт не совпадает с продуктом из таблицы Products
													{
														$z = $percent1;
													}
												}
												
												$allsummapercent[] = $summaOrder*$z;
											}

											//если менеджер равен определенному сотруднику, то считается процент
											elseif($cons1 == $arOr[$p]['manager'])
											{
												$percent1 = $percent/100;
												for($cx = 0; $cx <count($Products); $cx ++)//перебираем массив с данными из таблицы orders
												{
													if ($Products[$cx]["product"] == $productOrder)
													{
														$man = $Products[$cx]['manager_percent']/100;
														break;
														
													}
													else // если продукт не совпадает с продуктом из таблицы Products
													{
														$man = 0;
														$z = 0.1 - $percent1;
													}	
												}
												
												$allsummapercent[] = $summaOrder*$man;
											}
										}
									}
								}
							}
						}
					}
				}
					
				//Если есть оклад или определенный сотрудник это Эдуард Вартанян или Георгий Слизинтуев, то производим расчеты
				if (!empty($oklad) || $cons1 == "Эдуард Вартанян" || $cons1 == "Георгий Слизинтуев")
				{
					$n = @array_sum($allsummapercent) + $oklad + @array_sum($premSum);// складываем оклад и проценты
					$n1[] = $n; 
					$premia_obshay[] = @array_sum($premSum);// считаем бонусы консультанту за все месяца
					
					if($cons1 == "Эдуард Вартанян" || $cons1 == "Георгий Слизинтуев")//если определенный сотрудник это Эдуард Вартанян или Георгий Слизинтуев
					{
						$procob = $obSumSal1*4/100;// считаем общий процент(зарплата руководителя + премия, каждая по 2%)
						$sumobshay = $procob + $n;// получаем общую сумму
						$sumobshay1[] = $sumobshay;						
					}
				}			
			}
		
			if (!empty($oklad) || $cons1 == "Эдуард Вартанян" || $cons1 == "Георгий Слизинтуев")
			{
				$n2 = @array_sum($n1);
				
				if($cons1 == "Эдуард Вартанян" || $cons1 == "Георгий Слизинтуев")//если определенный сотрудник это Эдуард Вартанян или Георгий Слизинтуев
				{
					$sumobshay2 = @array_sum($sumobshay1);
					$ext = $sumobshay2 - $suma_viplat1;
					echo "<tr>";
					echo "<td class='th'>" . $cons1 . "</td>";// определенный сотрудник
					echo "<td class='th'>" . $ext . "</td>";// сумма ничислений
					echo "</tr>";
				}
				else// для всех остальных сотрудников у кого есть оклад, но не ставка(почасовая оплата)
				{
					$ext = $n2 - $suma_viplat1;
					echo "<tr>";
					echo "<td class='th'>" . $cons1 . "</td>";// определенный сотрудник
					echo "<td class='th'>" . $ext . "</td>";// сумма ничислений
					echo "</tr>";
				}
			}
		}
		
//--------------------------------Сейчас будет расчет для сотрудников у ставка(почасовая оплата)---------------------------------------------------------------------------------

		//Если нет оклада и определенный сотрудник не Эдуард Вартанян или Георгий Слизинтуев, то производим расчеты
		if (($cons1 != "Эдуард Вартанян") AND ($cons1 != "Георгий Слизинтуев") AND (empty($oklad)))
		{
			
			for ($as = 0; $as < count($work1); $as++)//перебираем все месяца
			{
				if ($work1[$as]["specialist"] == $cons1)
				{
					$work[] = $work1[$as];
				}
			}
			
			$rate = $employees[$ee]['rate'];// помещаем в переменную часовую ставку

			for ($w = 0; $w < count($work); $w ++)
			{
				$task = $work[$w]['task'];//добавляем в переменную название продукта из таблице complited_work
				$time = $work[$w]['time'];//добавляем в переменную время из таблице complited_work
				$charged =	$rate / 60;//добавляем в переменную значение которое сотрудник получает за 1 мин, т.е. руб/мин,(например, 180р/ч делим на 60 мин = 3 р/мин)
				$time1 = explode(":", $work[$w]['time']);//добавляем в переменную отдельно часы и минуты
				$timeSum [] = $time1[0] * 60 + $time1[1];//добавляем в массив преобразованное всё время в минуты (например 3:40 => 3*60 + 40 = 220 минут)
				$timeCharged[] = ($time1[0] * 60 + $time1[1]) * $charged;// добавляем в массив сумму которую получил сотрудник за определённый продукт (например, 3р/мин * 220 мин = 660 рублей)
			}
			$timeSum = @array_sum($timeSum);//сумма всего времени по продуктам за месяц определённого сотрудника
			$timeSumInt = intval($timeSum/60);//делим всю сумму времени на 60, что бы выделить часы
			$ostatocTime = $timeSum % 60;//делим всю сумму времени и берём остаток
			if ($ostatocTime < 10)// если минуты меньше 10, то добавляем ноль в переди. Это для корректного отображения времени
			{
				$ostatocTime = "0" . $ostatocTime;//добавляем ноль(например, 2:5 => 2:05)
			}
			$timeAllSum = $timeSumInt . ":" . $ostatocTime;//соединяем 0 и число (например, 2:5 => 2:05)

			$ext = @array_sum($timeCharged) - $suma_viplat1;
			echo "<tr>";
			echo "<td class='th'>" . $cons1 . "</td>";// определенный сотрудник
			echo "<td class='th'>" . $ext . "</td>";// сумма ничислений
			echo "</tr>";
		}
	}
	echo "</table>";
	global $premia_obshay1;
	$premia_obshay1 = @array_sum($premia_obshay);// считаем бонусы всем консультантам за весь период
}
?>
</body>
</html>