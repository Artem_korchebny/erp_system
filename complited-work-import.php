<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="main.css">
    <meta name="robots" content="noindex,nofollow"/>
    <title>Импорт выполненной работы</title>
</head>
<body>
<div>
    <h1>Импорт выполненной работы</h1>
</div>

<div>
    <h3>Загрузка файла</h3>
</div>
<div>
<form action="" enctype="multipart/form-data" method="post">
    <p>Ввести дату:</p>
    <input name="dateWork" type="date" value="<?=date('Y-m-d');?>"><br><br>
    <input type="file" name="file"/>
    <input type="submit" value="Загрузить"/>
</form>

</div>

<?
include "functions-dump.php";
require_once "js/Classes/PHPExcel.php";
function getConnect()
{
    $db = mysqli_connect("localhost", "u0264176_erp", "vo6F3azO", "u0264176_erp");
    mysqli_query($db, "SET NAMES utf8");
    return $db;
}
?>
<?
function uploadsFilesXls()
{
    $date = $_POST['dateWork'];
    foreach ($_FILES as $file)//перебираем глобальный массив $_FILES
    {
        $ar=array();
        $upload = "kl_to/";
        $loadfile = $file['name'];
        $filepath = "kl_to/" . $loadfile;
          if ($file['error'] != 0)
          {
              $message = "Произошла ошибка: " . $file['error'] . "!";
          }
          else
          {
            $path = $file['tmp_name'];
            $original = $upload . $file['name'];
            move_uploaded_file($path, $original);
            $inputFileType = PHPExcel_IOFactory::identify($filepath);  // узнаем тип файла, excel может хранить файлы в разных форматах, xls, xlsx и другие
            $objReader = PHPExcel_IOFactory::createReader($inputFileType); // создаем объект для чтения файла
            $objPHPExcel = $objReader->load($filepath); // загружаем данные файла в объект
            $ar = $objPHPExcel->getActiveSheet()->toArray(); // выгружаем данные из объекта в массив
          }
          for($i = 2; $i < count($ar); $i ++)
          {
              $db = getConnect();
              $specialist = $ar[$i][0];
              $task = $ar[$i][1];

              $timePlanMinutes  = explode(":", $ar[$i][2])[0] * 60 + explode(":", $ar[$i][2])[1];//преобразовали в минуты

              $timeWeekMinutes = explode(":", $ar[$i][3])[0] * 60 + explode(":", $ar[$i][3])[1];//преобразовали в минуты

              $timeAllMinutes = explode(":", $ar[$i][4])[0] * 60 + explode(":", $ar[$i][4])[1];//преобразовали в минуты

              $sumNew = preg_replace('/\s/', '', $sum);//удаление лишних пробелов(было 81 300, стало 81300)



            if ($timePlanMinutes == 0 || $timeAllMinutes <= $timePlanMinutes)
            {
                $intHous = intval($timeWeekMinutes/60);
                $ostatokMinutes = $timeWeekMinutes % 60;
                if($ostatokMinutes < 10)
                {
                    $ostatokMinutes = "0" . $ostatokMinutes;
                }
                $timeWeek = $intHous . ":" . $ostatokMinutes;
                $queryWork = "INSERT INTO complited_work (date, specialist, task, time) VALUES ('$date', '$specialist','$task', '$timeWeek')";
                mysqli_query($db, $queryWork);
            }
            $raznostAllandPlan = $timeAllMinutes - $timePlanMinutes;//разность всего затраченного времени и планируемые затраты
            if($raznostAllandPlan > 0)
            {
                if ($raznostAllandPlan > $timeWeekMinutes && $timePlanMinutes != 0)
                {
                    $timeWeek = "00:00";
                    $queryWork = "INSERT INTO complited_work (date, specialist, task, time) VALUES ('$date', '$specialist','$task', '$timeWeek')";
                    mysqli_query($db, $queryWork);
                }
                elseif($raznostAllandPlan < $timeWeekMinutes && $timePlanMinutes != 0)
                {
                    $raznostWeek = $timeWeekMinutes - $raznostAllandPlan;
                    $intHousWeek = intval($raznostWeek/60);
                    $ostatokWeekMinutes = $raznostWeek % 60;
                    if($ostatokWeekMinutes < 10)
                    {
                        $ostatokWeekMinutes = "0" . $ostatokWeekMinutes;
                    }
                    $timeAllWeek = $intHousWeek . ":" . $ostatokWeekMinutes;
                    $queryWork = "INSERT INTO complited_work (date, specialist, task, time) VALUES ('$date', '$specialist','$task', '$timeAllWeek')";
                    mysqli_query($db, $queryWork);
                }
            }
          }
      }
//dump($ar);
}
if (!empty($_FILES))
{
    uploadsFilesXls();
}
?>
<p><a href = 'index.php'>На главную</a></p>
</body>
</html>
