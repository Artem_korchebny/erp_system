<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Оплаты</title>
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		<form method='post'>
		<table id="myTable">
<?php

include "functions-dump.php";
include "dbconnect.php";

/*------ВЫВОД ССЫЛОК С МЕСЯЦАМИ------*/
$db = getConnect();
$query1 = "SELECT month(date), year(date) FROM income";//делаем выборку всех записей из БД
$sql1 = mysqli_query($db, $query1);
while ($monthDate = mysqli_fetch_array($sql1))
{
	$proverka = $monthDate['month(date)'];//переменная для того что бы проверить пусто ли в БД
	$month[] = $monthDate['month(date)'];
}
$month12 = array_values(array_unique($month));//В данной переменной массив, со всеми месяцами которые есть в БД
	if (isset($_GET['operation']))
	{
	  $month = $_GET['operation'];
	}
	else
	{
	  $_GET['operation'] = date('m');// Если в GET ничего нет, то добавляем текущий месяц
	  $month = $_GET['operation'];
	}
$months = array( 1 => 'Январь' , 'Февраль' , 'Март' , 'Апрель' , 'Май' , 'Июнь' , 'Июль' , 'Август' , 'Сентябрь' , 'Октябрь' , 'Ноябрь' , 'Декабрь' ); // массив с русскими названиями месяцев
for($m = 1;$m <= 12; $m ++)
{
	for($k=0; $k < count($month12); $k++)
	{
		if($month12[$k] == $m)
		{
			$months_date = date($months[date($m)]);// данный блок кода нужен для того, чтобы выбранный месяц выделялся жирным шрифтом
			if ($month != $m)
			{
				$may = "<a href='?operation=" . $m ."'>" . $months_date . "</a>";
				echo $may;
			}
			else
			{
				$may = "<a id='videleno' href='?operation=" . $m ."'>" . $months_date . "</a>";
				echo $may;
			}

		}
	}
}
/*------/ВЫВОД ССЫЛОК С МЕСЯЦАМИ------*/

/*------ВЫВОД ШАПКИ ТАБЛИЦЫ------*/
echo "<br><br>";
//делаем проверку на то что переменная $proverka не пустая, если не пустая выводит шапку таблицы
if (!empty($proverka))
{
	echo "<thead>";
	echo "<tr>";
	echo "<th>Дата</th>";
	echo "<th>Плательщик</th>";
	echo "<th>Новый клиент</th>";
	echo "<th>Первая оплата</th>";
	echo "<th style='width: 90px;'>Номер счета</th>";
	echo "<th>Комментарий</th>";
	echo "<th>Продукт</th>";
	echo "<th>Консультант</th>";
	echo "<th>Ассистент</th>";
	echo "<th>ООО/ИПП</th>";
	echo "<th>Сумма</th>";
	echo "<th>Расходники</th>";
	echo "<th>Яндекс</th>";
	echo "<th>Источник</th>";
	echo "<th>Акт</th>";
	echo "</tr>";
	echo "</thead>";
}
/*------/ВЫВОД ШАПКИ ТАБЛИЦЫ------*/

/*
Тут будут выводиться данные из таблиц income, orders, customers в которых:
1. совпадают номера счётов из таблицы inome и orders
	1.1. если они совпали то нужно проверить на совпадение сумм.
	1.2. если суммы совпадают, то заполнить из таблицы orders поля "Продукт" и "Консультант".
	1.3. если строк несколько, то для каждой строки нужно добавить отдельную строку в таблице "Оплаты".
	1.4. если суммы не равны, то в таблице "Оплаты" в поле "Комментарий" нужно дописать "Сумма оплаты не совпадает с суммой в счете"
2. если не совпали то переходим к следующему циклу(находится ниже) для вывода не совпадающих номеров счетов.
*/
//делаем выборку всех записей за определённый месяц из БД, где month получаем из GET запроса


$NewClient = 0;// вводим переменную для подсчета новых клиентов за месяц
$productMas = array();// Объявляем $productMas массивом, для того чтобы ниже вывести данный в таблицу доход
$summaMas = array();// Объявляем $summaMas массивом, для того чтобы ниже вывести данный в таблицу доход

$queryActs = "SELECT * FROM acts";
$sqlActs = mysqli_query($db, $queryActs);
while ($rowActs = mysqli_fetch_array($sqlActs))
{
	$Acts[] = trim($rowActs['act_status']);//получаем все статусы актов
}

$queryProductNew = "SELECT product FROM products ORDER BY product";
$sqlProductNew = mysqli_query($db, $queryProductNew);
while ($rowProductNew = mysqli_fetch_array($sqlProductNew))
{
	$ProductNew[] = trim($rowProductNew['product']);//получаем все продукты
}

$queryCons = "SELECT name FROM employees";
$sqlCons = mysqli_query($db, $queryCons);
while ($rowCons = mysqli_fetch_array($sqlCons))
{
	$ConsNew[] = trim($rowCons['name']);//получаем все продукты
}

$query = "SELECT * FROM income WHERE month(date)= '$month' ORDER BY date ASC";
$sql = mysqli_query($db, $query);
while ($row = mysqli_fetch_array($sql))
{
	$arSum[] = $row;//Добавляем в массив все данные за определённый месяц из таблицы income
	$sumRow = trim($row['order_schet']);//добавляем в переменную данные из order_schet

	$schekSum = "SELECT * FROM orders WHERE num = '$sumRow'";//выбираем все данные из таблицы orders, где num = номеру счёта из income
	$schekOrder =  mysqli_query($db, $schekSum);
	while($oRow = mysqli_fetch_array($schekOrder))
	{
		$arOr[] = $oRow;//добавляем в массив все данные, где num = $sumRow
		$arOr1[] = $oRow['num'];
		$fPay = array_unique($arOr1);
	}

	if ($row["date"] != null)//если date в массиве не пустая
	{
		$innlegal = $row["payer_inn"];//добавляем в переменную инн из таблицы income
		$legalQuery = "SELECT * FROM customers WHERE inn = '$innlegal'";//выбираем все записи из таблицы customers, где инн income равен инн customers
		$sqllegal = mysqli_query($db, $legalQuery);
		while ($rowCustomers = mysqli_fetch_array($sqllegal))
		{
			$masAllsummOrders = array();//создаём массив в который будет занасится сумма, консультан, продукт за определённый счёт
			for ($i = 0; $i < count($arSum); $i ++)
			{
				$sr = $row['order_schet'];
				$sumP = 0;
				for($j = 0; $j <count($arOr); $j ++)
				{
					$or = $arOr[$j]['num'];
					if($sr == $or)
					{
						$c = $arOr[$j]['consultant'];
						$p = $arOr[$j]['product'];
						$s = $arOr[$j]['sum'];
						$e = $arOr[$j]['expense'];
						$yan = $arOr[$j]['yandex'];
						$sumP = $sumP + (int)$s + (int)$e + (int)$yan;// $e прибавляем для того чтобы, в таблице выводились записи (т.к. у нас условие что запись выводится при равной сумме из income и orders)
					}
				}
				if($sumP != 0)
				{
					$masAllsummOrders[$sr] = array("$sumP", $c, $p);//поместили в массив сумму, консультанта, продукт в котором ключь это номер счёта из таблицы income
				}
				$sr = $arSum[$i]['order_schet'];
				foreach ($masAllsummOrders as $key => $value)
				{
					if ($sr == $key)
					{
						$t =  $arSum[$i]['sum'];
						if ($t == $value[0])
						{
							for ($p = 0; $p < count($arOr); $p++)
							{
								if ($row["order_schet"] == $arOr[$p]['num'])
								{
									//Начало строки
									echo "<tr>";
									//------ДАТА------
										echo "<td class='th d'>" . $row["date"] . "</td>";
									//------/ДАТА------

									//------ИМЯ ПЛАТЕЛЬЩИКА------
										//если коммерческое имя существует, то оно и выводиться
										if ($rowCustomers["commercial_name"] != NULL)
										{
											echo "<td class='th'>" . $rowCustomers["commercial_name"];
											echo "<br><span class='small'>" . $rowCustomers["legal_name"] . "</span></td>";

										}
										else
										{
											if ($arOr[$p]["customers"] == null)//если коммерческое имя не существует, то выводиться имя из таблицы orders(если оно есть)
											{
												echo "<td class='yellow'>" . $rowCustomers["legal_name"] . "</td>";

											}
											else// если в orders нет имени, то выводится юридическое имя из таблицы customers
											{
												echo "<td class='th'>" . $arOr[$p]["customers"] . "</td>";

											}
										}
									//------/ИМЯ ПЛАТЕЛЬЩИКА------

									//------НОВЫЙ КЛИЕНТ------
									//если значение в столбце new = 0 ,то выводим "Нет", если new=1, то выводим "Да"
										if ($row["new_customers"] == 0)
										{
											echo "<td class='th'>Нет</td>";
										}
										else// Если "Да", то добавляем в массив имя плательщика, для подсчета новых клиентов
										{

											if ($rowCustomers["commercial_name"] != NULL)
											{
												$Platelshik[] = $rowCustomers["commercial_name"];
											}
											else
											{
												if ($arOr[$p]["customers"] == null)
												{
													$Platelshik[] = $rowCustomers["legal_name"];
												}
												else
												{
													$Platelshik[] = $arOr[$p]["customers"];
												}
											}
											echo "<td class='th'>Да</td>";
										}
									//------/НОВЫЙ КЛИЕНТ------

									//------ПЕРВАЯ ОПЛАТА------
										if ($arOr[$p]['first_pay'] == 0)
										{
											echo "<td class='th'>Нет</td>";
										}
										else
										{
											echo "<td class='th'>Да</td>";
										}
									//------/ПЕРВАЯ ОПЛАТА------

									//------НОМЕР СЧЁТА------
										$rowschet = explode("-", $row["order_schet"])[1];//добавляем в переменную номер счёта без "15-";
										$link = explode("-", $row["order_schet"])[0];
										if ($link == "15")
										{
											echo "<td class='th'><a href='https://mmit.bitrix24.ru/crm/invoice/show/" . $rowschet . "/' target='_blank'>" . $row["order_schet"] . "</a></td>";
										}
										else
										{
											echo "<td class='yellow'>" . $row["order_schet"] . "</td>";//если номер счета не опознал, то ячейка окрашивается в желтый
										}
									//------/НОМЕР СЧЁТА------

									//------КОММЕНТАРИЙ и ПРОДУКТ------
										$keyOrder = $key;
										$valueNum = $arOr[$p]['num'];
										$legalNameCustomers = trim($rowCustomers["legal_name"]);
										if ($keyOrder == $valueNum)
										{

											if($arOr[$p]['product'] == null){
												$productOrder = $arOr[$p]['appointment'];
											}
											else{
												$productOrder = $arOr[$p]['product'];
											}

											$queryProducts = "SELECT product FROM products WHERE product = '$productOrder'";
											$sqlProducts = mysqli_query($db, $queryProducts);
											$appointment = $arOr[$p]['appointment'];

											if(isset($_POST['product'])){
											for($n=0; $n<count($ProductNew); $n++)
												{
													$prod = preg_quote($ProductNew[$n]);
													if(preg_match("($prod)", "$appointment"))
							                        {
														$queryAddP = "UPDATE orders SET product = '$prod' WHERE appointment = '$appointment'";
							                            mysqli_query($db, $queryAddP);
													}
													if(preg_match("(Обслуживание сайта)", "$appointment")){
														$queryAddP = "UPDATE orders SET manager = 'Алиса Сазонова' WHERE appointment = '$appointment'";
							                            mysqli_query($db, $queryAddP);
													}
												}
											}

											$queryContracts = "SELECT * FROM contracts WHERE customer = '$legalNameCustomers' AND product = '$productOrder'";
											$sqlContracts = mysqli_query($db, $queryContracts);
											while ($rowContractNum = mysqli_fetch_array($sqlContracts))
											{
												$numContract = $rowContractNum['number'];

											}
											//если есть схожий продукт, то проверяем, если есть договор для такого продукта, то выводим комментарий пустой, а продукт обычный
											//если есть схожий продукт, то проверяем, если нету договора для такого продукта, то в комментариий пишем нет договора, а продукт окрашиваем в жёлтый
											//если нет схожего продукта, то проверяем, если есть договор для такого продукта, то выводим комментарий пустой, а продукт желтый
											//если нет схожего продукта, то проверяем, если нету договора для такого продукта,то то в комментариий пишем нет договора, а продукт окрашиваем в жёлтый

											if (mysqli_num_rows($sqlProducts) > 0)
											{
												if(mysqli_num_rows($sqlContracts) > 0){
													echo "<td class='th'></td>";
													echo "<td class='th'>";

													if ($keyOrder == $valueNum)
													{
														$summaMas1 = $arOr[$p]['sum'];
													}

													if(in_array("$productOrder", $productMas))// Если в массиве $productMas есть значение $productOrder, то выполняем следующее
													{
														$key1 = array_search("$productOrder", $productMas);// // получаем ключ, в котором такой же продукт
														$obs = $summaMas[$key1] + $summaMas1;// складываем суммы одинаковых продуктов
														$summaMas[$key1] = $obs;// присваиваем существующему продукту сумму одинаковых продуктов
													}
													else// если в массиве такого значения нет, то добавляем значения в массивы
													{
														$productMas[] = $productOrder;
														$summaMas[] = $summaMas1;
													}

													echo $productOrder;
													echo "<br>";
													echo $numContract;
													$proShet = $arOr[$p]['num'];
													$appointment = $arOr[$p]['appointment'];
													$proNew = $arOr[$p]['id'] . "p";
													$proNewPost = $_POST[$proNew];
													echo "<select name='". $proNew ."'>
															<option></option>";

													for ($pro = 0; $pro < count($ProductNew); $pro ++)
													{
														$newPro = $ProductNew[$pro];
														echo "<option>" . $newPro . "</option>";
													}
													echo "</select>";
													if(isset($_POST['prov']) && !empty($proNewPost))
													{
														$queryNewPro = "UPDATE orders SET product = '$proNewPost' WHERE num = '$proShet' AND appointment = '$appointment'";
														mysqli_query($db, $queryNewPro);
														echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
													}
													echo "</td>";

												}
												else
												{
													echo "<td class='th'>Нет договора!</td>";
													echo "<td class='yellow'>";
													if ($keyOrder == $valueNum)
													{
														$summaMas1 = $arOr[$p]['sum'];
													}

													if(in_array("$productOrder", $productMas))// Если в массиве $productMas есть значение $productOrder, то выполняем следующее
													{
														$key1 = array_search("$productOrder", $productMas);// // получаем ключ, в котором такой же продукт
														$obs = $summaMas[$key1] + $summaMas1;// складываем суммы одинаковых продуктов
														$summaMas[$key1] = $obs;// присваиваем существующему продукту сумму одинаковых продуктов
													}
													else// если в массиве такого значения нет, то добавляем значения в массивы
													{
														$productMas[] = $productOrder;
														$summaMas[] = $summaMas1;
													}

													echo $productOrder;
													$proShet = $arOr[$p]['num'];
													$appointment = $arOr[$p]['appointment'];
													$proNew = $arOr[$p]['id'] . "p";
													$proNewPost = $_POST[$proNew];
													echo "<select name='". $proNew ."'>
															<option></option>";

													for ($pro = 0; $pro < count($ProductNew); $pro ++)
													{
														$newPro = $ProductNew[$pro];
														echo "<option>" . $newPro . "</option>";
													}
													echo "</select>";
													if(isset($_POST['prov']) && !empty($proNewPost))
													{
														$queryNewPro = "UPDATE orders SET product = '$proNewPost' WHERE num = '$proShet' AND appointment = '$appointment'";
														mysqli_query($db, $queryNewPro);
														echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
													}
													echo "</td>";
												}
											}
											else
											{
												if(mysqli_num_rows($sqlContracts) > 0){
													echo "<td class='th'></td>";
													echo "<td class='yellow'>";
													if ($keyOrder == $valueNum)
													{
														$summaMas1 = $arOr[$p]['sum'];
													}

													if(in_array("$productOrder", $productMas))// Если в массиве $productMas есть значение $productOrder, то выполняем следующее
													{
														$key1 = array_search("$productOrder", $productMas);// // получаем ключ, в котором такой же продукт
														$obs = $summaMas[$key1] + $summaMas1;// складываем суммы одинаковых продуктов
														$summaMas[$key1] = $obs;// присваиваем существующему продукту сумму одинаковых продуктов
													}
													else// если в массиве такого значения нет, то добавляем значения в массивы
													{
														$productMas[] = $productOrder;
														$summaMas[] = $summaMas1;
													}

													echo $productOrder;
													$proShet = $arOr[$p]['num'];
													$appointment = $arOr[$p]['appointment'];
													$proNew = $arOr[$p]['id'] . "p";
													$proNewPost = $_POST[$proNew];
													echo "<select name='". $proNew ."'>
															<option></option>";

													for ($pro = 0; $pro < count($ProductNew); $pro ++)
													{
														$newPro = $ProductNew[$pro];
														echo "<option>" . $newPro . "</option>";
													}
													echo "</select>";
													if(isset($_POST['prov']) && !empty($proNewPost))
													{
														$queryNewPro = "UPDATE orders SET product = '$proNewPost' WHERE num = '$proShet' AND appointment = '$appointment'";
														mysqli_query($db, $queryNewPro);
														echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
													}
													echo "</td>";
												}
												else
												{
													echo "<td class='th'>Нет договора!</td>";
													echo "<td class='yellow'>";
													if ($keyOrder == $valueNum)
													{
														$summaMas1 = $arOr[$p]['sum'];
													}

													if(in_array("$productOrder", $productMas))// Если в массиве $productMas есть значение $productOrder, то выполняем следующее
													{
														$key1 = array_search("$productOrder", $productMas);// // получаем ключ, в котором такой же продукт
														$obs = $summaMas[$key1] + $summaMas1;// складываем суммы одинаковых продуктов
														$summaMas[$key1] = $obs;// присваиваем существующему продукту сумму одинаковых продуктов
													}
													else// если в массиве такого значения нет, то добавляем значения в массивы
													{
														$productMas[] = $productOrder;
														$summaMas[] = $summaMas1;
													}

													echo $productOrder . "<br>";
													$proShet = $arOr[$p]['num'];
													$appointment = $arOr[$p]['appointment'];
													$proNew = $arOr[$p]['id'] . "p";
													$proNewPost = $_POST[$proNew];
													echo "<select name='". $proNew ."'>
															<option></option>";

													for ($pro = 0; $pro < count($ProductNew); $pro ++)
													{
														$newPro = $ProductNew[$pro];
														echo "<option>" . $newPro . "</option>";
													}
													echo "</select>";
													if(isset($_POST['prov']) && !empty($proNewPost))
													{
														$queryNewPro = "UPDATE orders SET product = '$proNewPost' WHERE num = '$proShet' AND appointment = '$appointment'";
														mysqli_query($db, $queryNewPro);
														echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
													}
													echo "</td>";
												}
											}
										}
									//------/КОММЕНТАРИЙ и ПРОДУКТ------

									//------КОНСУЛЬТАНТ------
									echo "<td class='th'>";
									$valueCons = $arOr[$p]['num'];
									if ($keyOrder == $valueCons)
									{
										$proShet = $arOr[$p]['num'];
										$appointment = $arOr[$p]['appointment'];
										$proCons = $arOr[$p]['id'] . "c";
										$proNewCons = $_POST[$proCons];
										echo "<select name='". $proCons ."'>
												<option></option>";

										for ($c = 0; $c < count($ConsNew); $c ++)
										{
											$newCons = $ConsNew[$c];
											echo "<option>" . $newCons . "</option>";
										}
										echo "</select>";
										if(isset($_POST['prov']) && !empty($proNewCons))
										{
											$queryCons = "UPDATE orders SET consultant = '$proNewCons' WHERE num = '$proShet' AND appointment = '$appointment'";
											mysqli_query($db, $queryCons);
											echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
										}
										$consultantOrder = $arOr[$p]['consultant'];
										echo $consultantOrder;
									}
									echo "</td>";
									//------/КОНСУЛЬТАНТ------

									//------АССИСТЕНТ------
									$Assistant = $arOr[$p]['assistant'];
									$Assistant_percent = $arOr[$p]['assistant_percent'];
									if ($Assistant == NULL)
									{
										echo "<td class='th'>";
										$proShet = $arOr[$p]['num'];
										$appointment = $arOr[$p]['appointment'];
										$proAssist = $arOr[$p]['id'] . "as";
										$proNewAssist = $_POST[$proAssist];

										$proPercent = $arOr[$p]['id'] . "per";
										$proNewPercent = $_POST[$proPercent];

										echo "<select name='". $proAssist ."'>
												<option></option>";

										for ($c = 0; $c < count($ConsNew); $c ++)
										{
											$newAssist = $ConsNew[$c];
											echo "<option>" . $newAssist . "</option>";
										}
										echo "</select><br><br>";
										echo "<input name=". $proPercent ."></input>";
										if(isset($_POST['prov']) && !empty($proNewAssist) && !empty($proNewPercent))
										{
											$queryAssist = "UPDATE orders SET assistant = '$proNewAssist', 	assistant_percent = '$proNewPercent' WHERE num = '$proShet' AND appointment = '$appointment'";
											mysqli_query($db, $queryAssist);
											echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
										}
										echo "</td>";
									}
									else
									{
										echo "<td class='th'>";
										$proShet = $arOr[$p]['num'];
										$appointment = $arOr[$p]['appointment'];
										$proAssist = $arOr[$p]['id'] . "as";
										$proNewAssist = $_POST[$proAssist];

										$proPercent = $arOr[$p]['id'] . "per";
										$proNewPercent = $_POST[$proPercent];

										echo "<select name='". $proAssist ."'>
												<option></option>";

										for ($c = 0; $c < count($ConsNew); $c ++)
										{
											$newAssist = $ConsNew[$c];
											echo "<option>" . $newAssist . "</option>";
										}
										echo "</select><br><br>";
										echo "<input name=". $proPercent ."></input>";
										if(isset($_POST['prov']) && !empty($proNewAssist) && !empty($proNewPercent))
										{
											$queryAssist = "UPDATE orders SET assistant = '$proNewAssist' WHERE num = '$proShet' AND appointment = '$appointment'";
											mysqli_query($db, $queryAssist);
											echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
										}
										echo $Assistant . " (" . $Assistant_percent . "%)";
										echo "</td>";
									}
									//------/АССИСТЕНТ------

									//------ООО/ИП------
									echo "<td class='th'>";
									$recipient = $row['account'];
									if ($recipient == 40802810818350019540)
									{
										$sumIpSov[] = $arOr[$p]['expense'];
										$sumYaIp[] = $arOr[$p]['yandex'];
										$rep = "ИП";
										echo $rep;
									}
									elseif ($recipient == 40702810818350002110)
									{
										$sumOOOSov[] = $arOr[$p]['expense'];
										$sumYaOOO[] = $arOr[$p]['yandex'];
										$rep = "ООО";
										echo $rep;
									}
									else{
										$sumNalRas[] = $arOr[$p]['expense'];
										$sumNalYa[] = $arOr[$p]['yandex'];
										$rep = "Нал";
										echo $rep;
									}
									echo "</td>";
									//------/ООО/ИП------

									//------СУММА------
									echo "<td class='th'>";
									if ($keyOrder == $valueNum)
									{
										$summaOrder = $arOr[$p]['sum'];
										echo $summaOrder;
									}
									echo "</td>";
									//------/СУММА------

									//------РАСХОДНИКИ и ЯНДЕКС------
									$numg = $arOr[$p]['id'];
									$n = $p . "a";
									if (isset($_POST['prov']))
									{
										$num55 = $arOr[$p]['num'];
										$pro55 = $arOr[$p]['appointment'];
										$nullRas = $arOr[$p]['expense'];
										$nullYa = $arOr[$p]['yandex'];
										$postvalye = $_POST[$n];
										$postvalyeYa = $_POST[$numg];
										if (!empty($postvalye))
										{
											$ss = $arOr[$p]['sum'] - ($postvalyeYa + $postvalye);
											$expenseBD = "UPDATE orders SET expense = '$postvalye', sum = '$ss' WHERE num = '$num55' AND appointment = '$pro55'";
											mysqli_query($db, $expenseBD);
											echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
										}
										else
										{
											if($postvalye == "0")
											{
												$ss = $arOr[$p]['sum'] + $nullRas;
												$expenseBD = "UPDATE orders SET expense = '$postvalye', sum = '$ss' WHERE num = '$num55' AND appointment = '$pro55'";
												mysqli_query($db, $expenseBD);
												echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
											}
										}
										if(!empty($postvalyeYa))
										{
											$ssYa = $arOr[$p]['sum'] - ($postvalyeYa + $postvalye);
											$yandexBD = "UPDATE orders SET yandex = '$postvalyeYa', sum = '$ssYa' WHERE num = '$num55' AND appointment = '$pro55'";
											mysqli_query($db, $yandexBD);
											echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
										}
										else
										{
											if($postvalyeYa == "0")
											{
												$ss = $arOr[$p]['sum'] + $nullYa;
												$expenseBD = "UPDATE orders SET yandex = '$postvalyeYa', sum = '$ss' WHERE num = '$num55' AND appointment = '$pro55'";
												mysqli_query($db, $expenseBD);
												echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
											}
										}
									}
									$uu[] = $arOr[$p]['expense'];
									echo "<td class='th cen'><input id='input' type='text' name='" . $n . "'><p class='ras'>" . $arOr[$p]['expense'] . "</p></td>";
									$ya[] = $arOr[$p]['yandex'];
									echo "<td class='th cen'><input id='input' type='text' name='" . $numg . "'><p class='ras'>" . $arOr[$p]['yandex'] . "</p></td>";
									//------/РАСХОДНИКИ и ЯНДЕКС------

									//------ИСТОЧНИК------
									if ($row["new_customers"] == 1)// Если поле "Новый клиент" = да, то выводим источник
									{
										if (($row["new_customers"] == 1) AND ($rowCustomers['source'] == NULL) )// Если в поле "Новый клиент" стоит "да", а поле "Источник" - пустое, то окрашивать ячейку "Источник" в жёлтый цвет
										{
											echo "<td class='yellow'>" . $rowCustomers['source'] . "</td>";
										}
										else
										{
											echo "<td class='th'>" . $rowCustomers['source'] . "</td>";
										}
									}
									elseif ($row["new_customers"] == 0)// Если поле "Новый клиент" = нет, то не выводим источник
									{
										echo "<td class='th'></td>";
									}
									//------/ИСТОЧНИК------

									//------АКТ------
										if($arOr[$p]["act"] == "Выполняется" || $arOr[$p]["act"] == "Акт отправлен на подпись"){
											$color = "blue";
										}
										elseif($arOr[$p]["act"] == "Выполнено"){
											$color = "red";
										}
										elseif($arOr[$p]["act"] == "Акт подписан" || $arOr[$p]["act"] == "Без акта"){
											$color = "white";
										}
										else{
											$color = "yellow";
										}
									    echo "<td id='acts' class='th $color'>";
										$actSchet = $arOr[$p]['num'];
										$productSchet = $arOr[$p]['appointment'];
										$actSchet1 = $arOr[$p]['id'] . "s";
										$actPost = $_POST[$actSchet1];
										echo "
										<select class='change_color' name='". $actSchet1 ."'>
												<option></option>";


										for ($ii = 0; $ii < count($Acts); $ii ++)
										{
											$Acts1 = trim($Acts[$ii]);

											echo "<option>" . $Acts1 . "</option>";
										}


										echo "</select>";

										echo "<br>" . $arOr[$p]["act"];
										echo "</td>";
									//------/АКТ------

									//Если нажата кнопка
										if(isset($_POST['prov']) && !empty($actPost))
										{
											$queryAct123 = "UPDATE orders SET act = '$actPost' WHERE num = '$actSchet' AND appointment = '$productSchet'";
											mysqli_query($db, $queryAct123);
											echo "<script type='text/javascript'>window.location = '". $_SERVER['HTTP_REFERER'] . "'</script>";
										}
									echo "</tr>";
									//Конец строки
								}
							}
						}
						else
						{
							echo "<tr>";
							echo "<td class='th d'>" . $row["date"] . "</td>";

							$schekSum1 = "SELECT * FROM orders WHERE num = '$sumRow'";//выбираем все данные из таблицы orders, где num = $sumRow
							$schekOrder1 =  mysqli_query($db, $schekSum1);
							while($oRow = mysqli_fetch_array($schekOrder1))
							{
								$custOredrs = $oRow["customers"];
							}
							//если коммерческое имя существует, то оно и выводиться
							if ($rowCustomers["commercial_name"] != NULL)
							{
								echo "<td class='th'>" . $rowCustomers["commercial_name"];
								echo "<br><span class='small'>" . $rowCustomers["legal_name"] . "</span></td>";
							}
							else
							{
								if ($arOr[$p]["customers"] == null)//если коммерческое имя не существует, то выводиться имя из таблицы orders(если оно есть)
								{
									echo "<td class='yellow'>" . $rowCustomers["legal_name"] . "</td>";
								}
								else// если в orders нет имени, то выводится юридическое имя из таблицы customers
								{
									echo "<td class='th'>" . $arOr[$p]["customers"] . "</td>";
								}
							}
							//если значение в столбце new = 0 ,то выводим "Нет", если new=1, то выводим "Да"
							if ($row["new_customers"] == 0)
							{
								echo "<td class='th'>Нет</td>";
							}
							else// Если "Да", то добавляем в массив имя плательщика, для подсчета новых клиентов
							{

								if ($rowCustomers["commercial_name"] != NULL)
								{
									$Platelshik[] = $rowCustomers["commercial_name"];
								}
								else
								{
									if ($arOr[$p]["customers"] == null)
									{
										$Platelshik[] = $rowCustomers["legal_name"];
									}
									else
									{
										$Platelshik[] = $arOr[$p]["customers"];
									}
								}
								echo "<td class='th'>Да</td>";
							}

							//вывод "да" или "нет", если в first_pay 1 или 0
							for($m = 0; $m < count($arOr); $m++)
							{

								//если номер счёта из income равн номеру счёта из orders, тогда проверяем на значение в first_pay
								if($row["order_schet"] == $fPay[$m])
								{
									if ($arOr[$m]['first_pay'] == 0)
									{
										echo "<td class='th'>Нет</td>";
									}
									else
									{
										echo "<td class='th'>Да</td>";
									}
								}
							}
							$rowschet = explode("-", $row["order_schet"])[1];
							$link = explode("-", $row["order_schet"])[0];
							if($link == "15")
							{
								echo "<td class='th'><a href='https://mmit.bitrix24.ru/crm/invoice/show/" . $rowschet . "/' target='_blank'>" . $row["order_schet"] . "</a></td>";
							}
							else
							{
								echo "<td class='yellow'>" . $row["order_schet"] . "</td>";// Сумма оплаты не совпадает с суммой в счете, то ячейка с суммой окрашивается в желтый
							}
							echo "<td class='th'>Сумма оплаты не совпадает с суммой в счете</td>";
							echo "<td class='th'></td>";
							echo "<td class='th'></td>";
							echo "<td class='th'></td>";
							echo "<td class='th'>";
							$recipient = $row['account'];
							if ($recipient == 40802810818350019540)
							{
								$rep = "ИП";
								echo $rep;
							}
							elseif ($recipient == 40702810818350002110)
							{
								$rep = "ООО";
								echo $rep;
							}
							else{
								$rep = "Нал";
								echo $rep;
							}
							echo "</td>";
							echo "</td>";
							echo "<td class='yellow'>" . $row["sum"] . "</td>";
							echo "<td class='th'></td>";
							echo "<td class='th'></td>";
							if ($row["new_customers"] == 1)// Если поле "Новый клиент" = да, то выводим источник
							{
								if (($row["new_customers"] == 1) AND ($rowCustomers['source'] == NULL) )// Если в поле "Новый клиент" стоит "да", а поле "Источник" - пустое, то окрашивать ячейку "Источник" в жёлтый цвет
								{
									echo "<td class='yellow'>" . $rowCustomers['source'] . "</td>";
								}
								else
								{
									echo "<td class='th'>" . $rowCustomers['source'] . "</td>";
								}
							}
							elseif ($row["new_customers"] == 0)// Если поле "Новый клиент" = нет, то не выводим источник
							{
								echo "<td class='th'></td>";
							}
							echo "<td class='th'></td>";
							echo "</tr>";
						}
					}
				}
			}
		}
	}
}


//Тут написан цикл который выводит не совпадающие номера счетов из income и orders

//Выбираем все записи из таблицы Orders
$queryOrders = "SELECT * FROM orders";
$sqlOrders = mysqli_query($db, $queryOrders);
while ($rowOrders = mysqli_fetch_array($sqlOrders))
{
	$numOrders[]= $rowOrders["num"];//помещаем в массив все номера счетов $kva = $numOrders
	$firstPay[] = $rowOrders;
}

//выбираем все записи из income за определённый месяц и сортируем их по дате
$queryAllmonth = "SELECT * FROM income WHERE month(date)= '$month' ORDER BY date ASC";
$sqlAllmonth = mysqli_query($db, $queryAllmonth);
while ($rowAllmonth = mysqli_fetch_array($sqlAllmonth))
{
	$allOr[] = $rowAllmonth;
	$OrderAll[] = $rowAllmonth['order_schet'];//помещаем в массив все номера счетов $kva1 = $OrderAll
	$allSumm[] = $rowAllmonth["sum"];
	$recipient = $rowAllmonth['account'];

	//условие для суммы ИП и ООО
	if ($recipient == 40802810818350019540)
	{
		$sumIp[] = $rowAllmonth["sum"];
		$rep = "ИП";
	}
	elseif ($recipient == 40702810818350002110)
	{
		$sumOOO[] = $rowAllmonth["sum"];
		$rep = "ООО";
	}
	else{
		$allSumNal[] = $rowAllmonth["sum"];
		$rep = "Нал";
		$rep;
	}

	//помещаем в массив все суммы
}

//Находим все совпадение из массива $OrderAll, сбрасываем индексацию и помещаяем в массив result
@$result = array_values(array_intersect ($numOrders, $OrderAll));

//меняем ключь и значение местами в массиве OrderAll, для того что бы удалить одинаковые элементы
@$OrderAll = array_flip($OrderAll);

//создаем счётчик для того что бы перебрать массив $result и удалить одинаковые элементы из массива $OrderAll
for ($d = 0; $d < count($result); $d++)
{
	$progon = $result[$d];
	unset ($OrderAll[$progon]);
}

//сбрасываем индексаци и меняем местами ключь и значение обратно
@$OrderAll = array_values(array_flip($OrderAll));
for ($u = 0; $u < count($OrderAll); $u ++)
{
	$OrderNumIn = $OrderAll[$u];
	$queryOrderNum = "SELECT * FROM income WHERE order_schet = '$OrderNumIn' AND month(date)= '$month'";//выбираем из таблицы income данные, где order_schet = номеру из массива $OrderAll
	$sqlOrderNum = mysqli_query($db, $queryOrderNum);
	while ($rowOrderIn = mysqli_fetch_array($sqlOrderNum))
	{
		$innlegal = $rowOrderIn["payer_inn"];//добавляем в переменную инн из таблицы income

		//выбираем все записи из таблицы customers, где инн income равен инн customers
		$legalQueryCust = "SELECT * FROM customers WHERE inn = '$innlegal'";
		$sqllegalCust = mysqli_query($db, $legalQueryCust);
		while ($rowCust = mysqli_fetch_array($sqllegalCust))
		{
			echo "<tr>";
			echo "<td class='th d'>" . $rowOrderIn["date"] . "</td>";

			//если коммерческое имя пустое, то в таблицу добавляется юридическое имя и ячейка красится в желтый цвет

				if ($rowCust["commercial_name"] != NULL)
				{
					echo "<td class='th'>" . $rowCust["commercial_name"];
					echo "<br><span class='small'>" . $rowCust["legal_name"] . "</span></td>";
				}
				else
				{
					echo "<td class='yellow'>" . $rowCust["legal_name"] . "</td>";
				}

			//если значение в столбце new = 0 ,то выводим "Нет", если new=1, то выводим "Да"
			if ($rowOrderIn["new_customers"] == 0)
			{
				echo "<td class='th'>Нет</td>";
			}
			else// Если "Да", то добавляем в массив имя плательщика, для подсчета новых клиентов
			{
				if ($rowCust["commercial_name"] != NULL)
				{
					$Platelshik[] = $rowCust["commercial_name"];
				}
				else
				{
					$Platelshik[] = $rowCust["legal_name"];
				}
				echo "<td class='th'>Да</td>";
			}
			echo "<td class='th'></td>";
			$rowschet = explode("-", $rowOrderIn["order_schet"])[1];
			$link = explode("-", $rowOrderIn["order_schet"])[0];
			if (strpos($rowOrderIn["order_schet"], 'Дубль') !== false) //если в номере счета есть слово "Дубль", то ячейка окрашивается в желтый цвет
			{
				echo "<td class='yellow'><a href='https://mmit.bitrix24.ru/crm/invoice/show/" . $rowschet . "/' target='_blank'>" . $rowOrderIn["order_schet"] . "</a></td>";
			}
			elseif ($link == "15")
			{
				echo "<td class='th'><a href='https://mmit.bitrix24.ru/crm/invoice/show/" . $rowschet . "/' target='_blank'>" . $rowOrderIn["order_schet"] . "</a></td>";
			}
			else
			{
				echo "<td class='yellow'>" . $rowOrderIn["order_schet"] . "</td>";
			}
			echo "<td class='th'>Такого счета нет!</td>";
			echo "<td class='th'></td>";
			echo "<td class='th'></td>";
			echo "<td class='th'></td>";
			echo "<td class='th'>";
			$recipient = $rowOrderIn['account'];
			if ($recipient == 40802810818350019540)
			{
				$rep = "ИП";
				echo $rep;
			}
			elseif ($recipient == 40702810818350002110)
			{
				$rep = "ООО";
				echo $rep;
			}
			else{
				$rep = "Нал";
				echo $rep;
			}
			echo "</td>";
			echo "<td class='th'>" . $rowOrderIn["sum"] . "</td>";
			echo "<td class='th'></td>";
			echo "<td class='th'></td>";
			if ($rowOrderIn["new_customers"] == 1)// Если поле "Новый клиент" = да, то выводим источник
			{
				if (($rowOrderIn["new_customers"] == 1) AND ($rowCust['source'] == NULL) )// Если в поле "Новый клиент" стоит "да", а поле "Источник" - пустое, то окрашивать ячейку "Источник" в жёлтый цвет
				{
					echo "<td class='yellow'>" . $rowCust['source'] . "</td>";
				}
				else
				{
					echo "<td class='th'>" . $rowCust['source'] . "</td>";
				}
			}
			elseif ($rowOrderIn["new_customers"] == 0)// Если поле "Новый клиент" = нет, то не выводим источник
			{
				echo "<td class='th'></td>";
			}
			echo "<td class='th'></td>";
			echo "</tr>";
		}
	}
}


$nalSummRas = @array_sum($sumNalRas);
$nalSummYa = @array_sum($sumNalYa);


$allSumNal = @array_sum($allSumNal);
$SumAllNal = $allSumNal-$nalSummRas-$nalSummYa;
$allSumOOO = @array_sum($sumOOO) - @array_sum($sumOOOSov) - @array_sum($sumYaOOO);
$allSumIp = @array_sum($sumIp) - @array_sum($sumIpSov) - @array_sum($sumYaIp);
$nalSum = @array_sum($sumNal);
$sumItogo = @array_sum($allSumm);
$sqlSum = @array_sum($uu);
$sqlYa = @array_sum($ya);
$sumAll = $sumItogo - $sqlSum - $sqlYa;
//делаем проверку на то что переменная $proverka не пустая для вывода объёма продаж
if (!empty($proverka))
{
	echo "<thead>"; //оформляем последнюю строку таблицы в теги "<thead>" для того чтобы эта строка не поддавалась сортировке
	echo "<tr>";
	echo "<td class='th' colspan='9' rowspan='4'>Итого:</td>";
	echo "<td class='th'></td>";
	echo "<td class='th' >" . $sumAll . "</td>";// с помощью "array_sum" выводим сумму всех значений массива
	echo "<td id='f'  class='th' >" . @$sqlSum . "</td>";// с помощью "array_sum" выводим сумму всех значений массива
	echo "<td class='th'>" . @$sqlYa . "</td>";
	echo "<td class='th'></td>";
	echo "</tr>";

	echo "<tr>";
	echo "<td class='th'>ООО:</td>";
	echo "<td class='th' >" . $allSumOOO . "</td>";// с помощью "array_sum" выводим сумму всех значений массива
	echo "<td class='th' >" . @array_sum($sumOOOSov) . "</td>";// с помощью "array_sum" выводим сумму всех значений массива
	echo "<td class='th'>" . @array_sum($sumYaOOO) . "</td>";
	echo "<td class='th'></td>";
	echo "</tr>";

	echo "<tr>";
	echo "<td class='th'>ИП:</td>";
	echo "<td class='th' >" . $allSumIp . "</td>";// с помощью "array_sum" выводим сумму всех значений массива
	echo "<td class='th' >" . @array_sum($sumIpSov) . "</td>";// с помощью "array_sum" выводим сумму всех значений массива
	echo "<td class='th'>" . @array_sum($sumYaIp) . "</td>";
	echo "<td class='th'></td>";
	echo "</tr>";

	echo "<tr>";
	echo "<td class='th'>Нал:</td>";
	echo "<td class='th' >" . $SumAllNal . "</td>";// с помощью "array_sum" выводим сумму всех значений массива
	echo "<td class='th' >" . $nalSummRas . "</td>";// с помощью "array_sum" выводим сумму всех значений массива
	echo "<td class='th'>" . $nalSummYa . "</td>";
	echo "<td class='th'></td>";
	echo "</tr>";
	echo "</thead>";
}
?>
</table>
<?php
$result = @array_unique($Platelshik);// удаляем повторяющихся плательщиков
$result1 = count($result);// считаем количество новых клиентов
echo  "Новых клиентов за месяц:" . $result1;
echo "<p><input type='submit' name='prov' value='Сохранить'></p>";
echo "<p><input type='submit' name='product' value='Обновить продукты'></p>";
echo "</form>";
//Скрип для отправки данных из таблице в Google таблицу добавляем в переменную $ex значение из GET['operation']
$ex = $_GET['operation'];

//добавляем в переменную $googleFunct функцию гугле таблицы, которая инпортирует данные из таблице или списка указанной web-странице
$googleFunct = '=IMPORTHTML("http://erp.1cbmmit.ru/income.php?operation=' . $ex . '"; "table"; 1)';
echo "<p>";
echo "<form action='https://script.google.com/macros/s/AKfycbxE2b5dxO3MYiRETNQPKyojCcDD-bFXd9XL9xD8a3S6xtGP5F8/exec?p1=SUCCESS&p2=Server1%20is%20complete' method='get'>";
echo "<input type='hidden' name='p2' value='" . $googleFunct . "'>";

// $ex мы передаем в качестве параметра p1. Значение p1 подставлено в гугл-скрипт, который прописан в настройках нашей гугл таблицы. Данный скрипт привожу ниже.

// function doGet(e)
// {
// var sheet = SpreadsheetApp.openById("1edcjb0sPiuXPKd6OkVYbVvdMCYrflZdWBh0YlStVgVA");
//  var ss = SpreadsheetApp.getActiveSpreadsheet();
//  var sheet = ss.getSheets()[e.parameter.p1];
//  sheet.deleteRow(1);
//  sheet.getRange("A1").setValue(e.parameter.p2);
// return HtmlService.createHtmlOutput(
// "<script>window.top.location.href='http://erp.1cbmmit.ru/income.php';</script>"//
//  );
//  }

// p1 в данной функции определяет номер листа страницы (отсчет начинается с 0). Поэтому когда мы, например, выбираем январь, в p1 должно быть значение 0, т.д.
// Для этого прописано условие ниже.

if ($ex == '01')
{
	$ex = '0';
}
elseif ($ex == '02')
{
	$ex = '1';
}
elseif ($ex == '03')
{
	$ex = '2';
}
elseif ($ex == '04')
{
	$ex = '3';
}
elseif ($ex == '05')
{
	$ex = '4';
}
elseif ($ex == '06')
{
	$ex = '5';
}
elseif ($ex == '07')
{
	$ex = '6';
}
elseif ($ex == '08')
{
	$ex = '7';
}
elseif ($ex == '09')
{
	$ex = '8';
}
elseif ($ex == '10')
{
	$ex = '9';
}
elseif ($ex == '11')
{
	$ex = '10';
}
elseif ($ex == '12')
{
	$ex = '11';
}

// Передаем в качестве значения p1 переменную $ex, для того чтобы наши данные попали на нужный лист таблицы
echo "<input type='hidden' name='p1' value='" . $ex . "'>";
echo '<input type="submit" id="google" value="Актуализировать Google таблицу">
<a class="tableGoole" href = "https://docs.google.com/spreadsheets/d/1edcjb0sPiuXPKd6OkVYbVvdMCYrflZdWBh0YlStVgVA/edit#gid=0" target="_blank">Google таблица</a>';
echo "</form>";
echo "</p>";
echo "<p><a href = 'index.php'>На главную</a></p>";
?>

<hr>


<?
if (@array_sum($summaMas) > 0 )// если массив $summaMas > 0 , то выводим шапку таблицы
{
	echo "<h3>Доход</h3>";
	echo "<table id='myTable1'>";
	echo "<thead>";
    echo "<tr>";
   	echo "<th>Продукт</th>";
   	echo "<th>Сумма</th>";
	echo "<th>%</th>";
   	echo "</tr>";
	echo "</thead>";
}

$allsummaMas = @array_sum($summaMas);// считаем общую сумму, всех продуктов(она нужна для вычисления процентов)

for($i = 0; $i < count($productMas); $i ++)// перебираем массивы, вытягивая значения и выводя их в таблицу
{
	$productMas2 = $productMas[$i];
    $summaMas2 = $summaMas[$i];
	$procent = $summaMas2*100/$allsummaMas;//формула такая: сумму продукта умножаем на 100% и делим на общую сумму
	if ($summaMas2 > 0)// если $summaMas2 < 0 , то ничего выводить не нужно
	{
		echo "<tr>";
        echo "<td class='th'>" . $productMas2 . "</td>";
        echo "<td class='th'>" . $summaMas2 . "</td>";
		echo "<td class='th'>" . round($procent, 2) . "</td>";//round($procent, 2) - это для того чтобы после запятой было не больше 2 символов
		echo "</tr>";
	}
}
?>
</table>
<?
/*-----УДАЛЕНИЕ ЛИШНИХ ПРОБЕЛОВ------*/
if(isset($_POST['gg'])){
$queryOrderNum123 = "SELECT * FROM income";//выбираем из таблицы income данные, где order_schet = номеру из массива $OrderAll
$sqlOrderNum123 = mysqli_query($db, $queryOrderNum123);
while ($rowOrderIn123 = mysqli_fetch_array($sqlOrderNum123))
{
	$rowNew[] = $rowOrderIn123;
}
$queryOrderNum5= "SELECT * FROM customers";//выбираем из таблицы income данные, где order_schet = номеру из массива $OrderAll
$sqlOrderNum5 = mysqli_query($db, $queryOrderNum5);
while ($rowOrderIn5 = mysqli_fetch_array($sqlOrderNum5))
{
	$rowNew5[] = $rowOrderIn5;
}
for($i=0; $i < count($rowNew5); $i++){
	$id = $rowNew5[$i]['id'];
	$inn = trim($rowNew5[$i]['inn']);
	$legal_name = trim($rowNew5[$i]['legal_name']);
	$new = trim($rowNew5[$i]['new']);
	$source = trim($rowNew5[$i]['source']);
	$queryOrderNum6 = "UPDATE customers SET inn = '$inn', legal_name = '$legal_name', new = '$new', source = '$source' WHERE id = '$id'";//выбираем из таблицы income данные, где order_schet = номеру из массива $OrderAll
	mysqli_query($db, $queryOrderNum6);
}
for($i=0; $i < count($rowNew); $i++){
	$id = $rowNew[$i]['id'];
	$date = trim($rowNew[$i]['date']);
	$order_num = trim($rowNew[$i]['order_num']);
	$sum = trim($rowNew[$i]['sum']);
	$payer_inn = trim($rowNew[$i]['payer_inn']);
	$comment = trim($rowNew[$i]['comment']);
	$account = trim($rowNew[$i]['account']);
	$order_schet = trim($rowNew[$i]['order_schet']);
	$new_customers = trim($rowNew[$i]['new_customers']);
	$queryOrderNum4 = "UPDATE income SET date = '$date', order_num = '$order_num', sum = '$sum', payer_inn = '$payer_inn', comment = '$comment',
	account='$account', order_schet='$order_schet', new_customers = '$new_customers' WHERE id = '$id'";//выбираем из таблицы income данные, где order_schet = номеру из массива $OrderAll
	mysqli_query($db, $queryOrderNum4);
}
dump($_POST['gg']);
}
/*-----УДАЛЕНИЕ ЛИШНИХ ПРОБЕЛОВ------*/
?>
<form method="post">
<input type="submit" name="gg">
</form>
</body>
</html>
