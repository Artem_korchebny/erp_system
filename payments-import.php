<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Импорт выписки из банка</title>
</head>
<body>
<div >
    <div>
        <h1>Импорт Платежей</h1>
    </div>

    <div>
        <h3>Загрузка выписки из банка</h3>
    </div>

    <form action="" enctype="multipart/form-data" method="post">
        <input type="file" name="file"/>
        <input type="submit" value="Загрузить"/>
    </form>

<?php
//функция для соединения к БД и задаёт набор символов по умолчанию UTF8
function getConnect()
{
    $db = mysqli_connect("localhost", "u0264176_erp", "vo6F3azO", "u0264176_erp");
    mysqli_query($db, "SET NAMES utf8");
    return $db;
}
/*функция для загрузки файла на сервер +
выбираем данные, в данном случае:
  1.ПолучательСчет,
  2.Номер,
  3.Дата,
  4.ПлательщикИНН,
  5.Сумма,
  6.НазначениеПлатежа.
и заносим из в БД.
*/
function uploadsFiles()
{
    foreach ($_FILES as $file)//перебираем глобальный массив $_FILES
    {
	      $upload = "../erp.1cbmmit.ru/kl_to/";
        if ($file['error'] != 0)
        {
            $message = "Произошла ошибка: " . $file['error'] . "!";
        }
        else
        {
            $path = $file['tmp_name'];
            $original = $upload . $file['name'];
            move_uploaded_file($path, $original);//заменяем временную дирикторию загрузки файла на новую
            $lines = file($original);// добавляем в массив $lines данные из файла
        }
    }
    foreach ($lines as $key)
    {
      $valLeft = explode("=", $key)[0];//разбиваем строку на подсктроку, где разделитель =, и добавляем первое значение в переменную
      $valRight = explode("=", $key)[1];//разбиваем строку на подсктроку, где разделитель =, и добавляем второе значение в переменную

        if ($valLeft == "РасчСчет")//если находим "ПолучательСчет", то все значения добавляем в массив, и т.д.
        {
          $account = $valRight;
          $masAccount[] = array($account);
        }
        if ($valLeft == "Номер")
        {
          $order_num = $valRight;
          $masOrder[] = array($order_num);
        }
        if ($valLeft == "Дата")
        {
          $date = $valRight;
          $date_mas = explode(".", $date);
          $date_mas = array_reverse($date_mas);
          $date_new = implode("-", $date_mas);
          $date = preg_replace('/\s/', '', $date_new);
          $masDate[] = array($date);
        }
        if ($valLeft == "Сумма")
        {
          $sum = $valRight;
          $masSum[] = array($sum);
        }
        if ($valLeft == "ПлательщикИНН")
        {
          $payer_inn = $valRight;
          $masPayer[] = array($payer_inn);
        }
        if ($valLeft == "НазначениеПлатежа")
        {
          $comment = $valRight;
          $masComment[] = array($comment);
		      $ord = explode("15-", $valRight)[1];
		      $schet = substr($ord, 0, 4);
      		  if ($schet == 0)
      		  {
      			     $masSchet[] = array($comment);
      		  }
      		  else
      		  {
      			     $masSchet[] = array("15-" . $schet);
      		  }
        }
        if ($valLeft == "Плательщик")
        {
          $legalName = $valRight;
          $masLegal[] = array($legalName);
        }
        if ($valLeft == "ПолучательСчет")
        {
          $recipient = $valRight;
          $masRecipient[] = array($recipient);
        }
   }
//создаем счётчик, чтобы вытащить значения из выше расположенных массивов
for ($i = 0; $i < count($masOrder); $i++)
{
    $accountI = $masAccount[0][0];
    $orderI = $masOrder[$i][0];
    $dateI = $masDate[$i][0];
    $sumI = $masSum[$i][0];
    $payerI = $masPayer[$i][0];
    $commentI = $masComment[$i][0];
    $legalI = $masLegal[$i][0];
	  $schetI = $masSchet[$i][0];
    $recipientI = $masRecipient[$i][0];
//создаём новый массив и заносим данные по очерёдно
    $allMas[$i] =
    array(
      "РасчСчет" => "$accountI",
      "Номер" => "$orderI",
      "Дата" => "$dateI",
      "Сумма" => "$sumI",
      "ПлательщикИНН" => "$payerI",
      "НазначениеПлатежа" => "$commentI",
      "Плательщик" => "$legalI",
	    "Номер счета" => "$schetI",
      "ПолучательСчет" => "$recipientI",
    );
}
//создаём новый счётчик для выбора значений из массива $allMas, и заносим из в БД.
for ($i = 0; $i < count($allMas) ; $i++)
{
    $accountDB = trim($allMas[$i]["РасчСчет"]);
    $orderDB = trim($allMas[$i]["Номер"]);
    $dateDB = trim($allMas[$i]["Дата"]);
    $sumDB = trim($allMas[$i]["Сумма"]);
    $payerDB = trim($allMas[$i]["ПлательщикИНН"]);
    $commentDB = trim($allMas[$i]["НазначениеПлатежа"]);
    $legalDB = trim($allMas[$i]["Плательщик"]);
	$schetDB = trim($allMas[$i]["Номер счета"]);
    $recipientDB = trim($allMas[$i]["ПолучательСчет"]);
//если ПлательщикИНН = 381115999541, тогда мы не заносим в БД этот платёж
  if(("$payerDB" != 381115999541) && ("$payerDB" != 3849031156))
  {
      $db = getConnect();
      //выбираем месяц для проверки того, если в БД данные или нет
      $queryD = "SELECT month(date), year(date) FROM income";
      $sqlD = mysqli_query($db, $queryD);
      while ($monthDate = mysqli_fetch_array($sqlD))
      {
        $proverka =  $monthDate;//переменная для проверки содержания БД
      }
      //если переменная не пустая, тогда удаляем данные из БД по 4м уникальным полям, которые дублируются из файла
      if (!empty($proverka))
      {

      }
      //выбираем из БД данные по 4м уникальным полям
      $resProv = "SELECT * FROM `income` WHERE `date`='$dateDB' AND `order_num`='$orderDB'
                  AND `sum`='$sumDB' AND `payer_inn`='$payerDB'";
      $resUnic = mysqli_query($db, $resProv);

      $resCust = "SELECT * FROM customers WHERE `inn`='$payerDB'";
      $resCust = mysqli_query($db, $resCust);

      //выбираем Счета для проверки на уникальность
      $resOrder = "SELECT order_schet FROM income WHERE order_schet = '$schetDB'";
      $resOr = mysqli_query($db, $resOrder);

      //если есть схожие счета, добавляем в конец счёта "Дубль"
      if (mysqli_num_rows($resOr) > 0){
        $schetDB = $schetDB . "-Дубль";
        $or1 = "UPDATE income SET order_schet = '$schetDB' WHERE order_schet = '$schetDB'";
        mysqli_query($db, $or1);
      }
      $new_customers = 1;
      $new_customers1 = 0;
      //если такие записи уже существуют, возвращается 1, и не добавляется в БД
      if(mysqli_num_rows($resUnic) <= 0)
      {
          if(mysqli_num_rows($resCust) > 0)
          {
          $query = "INSERT INTO income (date, order_num, sum, payer_inn, comment, account, order_schet, new_customers)
                    VALUES ('$dateDB', '$orderDB', '$sumDB', '$payerDB', '$commentDB', '$accountDB', '$schetDB', $new_customers1)";
          mysqli_query($db, $query);
          }
          else{
            $query = "INSERT INTO income (date, order_num, sum, payer_inn, comment, account, order_schet, new_customers)
                      VALUES ('$dateDB', '$orderDB', '$sumDB', '$payerDB', '$commentDB', '$accountDB', '$schetDB', $new_customers)";
            mysqli_query($db, $query);
        }

      }
      else
      {
      }
      //подключаемся к БД, проверяем уникальность записей по 1 полю ПлательщикИНН.
      //если такие записи уже существуют, возвращается 1, и не добавляется в БД
      $legg[$payerDB] = $legalDB;
  }

}
foreach ($legg as $key => $value) {
  $legalInn = "SELECT * FROM `customers` WHERE `inn`='$key'";
  $leg5 = mysqli_query($db, $legalInn);
  $a = 1;
  if(mysqli_num_rows($leg5) > 0)
  {
    $queryLegal = "UPDATE customers SET new=0 WHERE `inn`='$key'";
    mysqli_query($db, $queryLegal);
  }
  else
  {
    $queryLegal = "INSERT INTO customers (inn, legal_name, new) VALUES ('$key', '$value', '$a')";
    mysqli_query($db, $queryLegal);
  }
}

}

//если глобальный массив не пустой, то выполняем функцию загрузки файла
if (!empty($_FILES))
{
    uploadsFiles();
}
echo "<p><a href = 'index.php'>На главную</a></p>";
?>
</div>
</body>
</html>
