<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Продажи по клиентам</title>
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		<table id="myTable">
<?
include "functions-dump.php";// подключаем функцию dump
include "dbconnect.php";// подключаем функцию getConnect с параметрами подключения к БД

$db = getConnect();// подключаемся к БД

$querycustomers = "SELECT * FROM customers WHERE source != ''";//извлекаем все записи из БД где в source есть данные 
$sqlcustomers = mysqli_query($db, $querycustomers);
if(mysqli_num_rows($sqlcustomers) > 0)// если в БД есть записи, то выводим шапку таблицы
{
	echo "<thead>";
		echo "<tr>";
			echo "<th>Источник</th>";
			echo "<th>Сумма</th>";
		echo "</tr>";
	echo "</thead>";
}

$sourceMass = array();// Объявляем $sourceMass массивом, для того чтобы ниже объединить суммы с одиноковым источником
$summaMas = array();// Объявляем $summaMas массивом, для того чтобы ниже объединить суммы с одиноковым источником
while ($rowCustomers = mysqli_fetch_array($sqlcustomers))
{
	$source = $rowCustomers["source"];
	$inn = $rowCustomers["inn"];
	$sum = array();// обнуляем массив, чтобы при последующем цикле в нем не было лишних данных
	$queryincome = "SELECT * FROM income WHERE payer_inn = '$inn'";// выводим все данные где ИНН из таблицы customers = ИНН из таблицы income
	$sqlincome = mysqli_query($db, $queryincome);
	while ($rowincome = mysqli_fetch_array($sqlincome))
	{
		$sum[] = $rowincome["sum"];// добавляем в массив все суммы с одинаковым ИНН
	}
	
	$sumOb = @array_sum($sum);// получаем общую сумму платежей за все время
	
	if(in_array("$source", $sourceMass))// Если в массиве $sourceMass есть значение $source, то выполняем следующее
	{
		$key1 = array_search("$source", $sourceMass);// // получаем ключ, в котором такой же источник
		$obs = $summaMas[$key1] + $sumOb;// складываем суммы где один источник
		$summaMas[$key1] = $obs;// присваиваем существующей сумме сумму всех где один источник
	}
	else// Если в массиве $sourceMass нет значение $source, то выполняем следующее
	{
		$sourceMass[] = $source;
		$summaMas[] = $sumOb;
	}	
}	
	for($i = 0; $i < count($sourceMass); $i ++)// перебираем массивы, извлекая значения и выводя их в таблицу
	{
		$sourceMass1 = $sourceMass[$i];
		$summaMas1 = $summaMas[$i];
		if ($summaMas1 > 0)// если $summaMas2 < 0 , то ничего выводить не нужно
		{
			echo "<tr>";
				echo "<td class='th'>" . $sourceMass1 . "</td>";
				echo "<td class='th'>" . $summaMas1 . "</td>";
			echo "</tr>";
		}
	}
?>

		</table>
		<br>
		<a href = 'index.php'>На главную</a>
	</body>
</html>