<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Продажи по клиентам</title>
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
    <table id="myTable">
<?
include "functions-dump.php";// подключаем функцию dump
include "dbconnect.php";// подключаем файл где прописано подключение к БД

$db = getConnect();
$querycustomers = "SELECT * FROM customers";//делаем выборку записей из БД
$sqlcustomers = mysqli_query($db, $querycustomers);
if(mysqli_num_rows($sqlcustomers) > 0)// если в БД есть записи, то выводим шапку таблицы
{
	echo "<thead>";
    echo "<tr>";
    echo "<th>Клиент</th>";
    echo "<th>Сумма</th>";
	echo "</tr>";
	echo "</thead>";
}
//пока есть записи они добавляется в массив
while ($rowCustomers = mysqli_fetch_array($sqlcustomers))
{
	$Customers[] = $rowCustomers;// добавили все данные в массив
}
for ($i = 0; $i < count($Customers); $i ++)// перебираем массив $Customers
{
	$Customers_inn = $Customers[$i]["inn"];
	$sum = array();// обнуляем массив, чтобы при последующем цикле в нем не было лишних данных
	$queryincome = "SELECT * FROM income WHERE payer_inn = '$Customers_inn'";// выводим все данные где ИНН из таблицы customers = ИНН из таблицы income
	$sqlincome = mysqli_query($db, $queryincome);
	while ($rowincome = mysqli_fetch_array($sqlincome))
	{
		$sum[] = $rowincome["sum"];// добавляем в массив все суммы с одинаковым ИНН
	}
	$sumOb = @array_sum($sum);// получаем общую сумму платежей за все время
	$Customers[$i]["sum"] = $sumOb;// записываем эту сумму в массив $Customers
}

$sum_all = array();
$name_all = array();

for ($w = 0; $w < count($Customers); $w ++)// перебираем массив $Customers
{
	$sum = $Customers[$w]["sum"];// сумма
	if($Customers[$w]["commercial_name"] != null)// если есть коммерческое имя, то берем его
	{
		$name = $Customers[$w]["commercial_name"];
	}
	else// если коммерческого имени нет, берем легальное имя
	{
		$name = $Customers[$w]["legal_name"];
	}
	if(in_array("$name", $name_all))// проверяем есть ли в массиве такое имя, если есть то
	{
		$key1 = array_search("$name", $name_all);// // получаем ключ, в котором такое же имя
		$obs = $sum_all[$key1] + $sum;// складываем суммы у одинаковых имен
		$sum_all[$key1] = $obs;// присваиваем существующему имени сумму одинаковых имен
	}
	else// если в массиве такого значения нет, то добавляем значения в массивы
	{
		$name_all[] = $name;
		$sum_all[] = $sum;
	}
}

for($e = 0; $e < count($name_all); $e ++)// перебираем массивы, вытягивая значения и выводя их в таблицу
{
	$name1 = $name_all[$e];
    $sum1 = $sum_all[$e];
	echo "<tr>";
	echo "<td class='th'>" . $name1 . "</td>";
	echo "<td class='th'>" . $sum1 . "</td>";
	echo "</tr>";
}
?>
</table>
<br>
<a href = 'index.php'>На главную</a>
</body>
</html>