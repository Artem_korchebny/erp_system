<?include "dbconnect.php";?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Ввести корректировки</title>
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
<body>
<?
$db = getConnect();

$queryVariables = "SELECT * FROM variables";//делаем выборку всех записей из БД
$sqlVariables = mysqli_query($db, $queryVariables);
while ($rowVariables = mysqli_fetch_array($sqlVariables))
{
  $variables[] = $rowVariables;
}
?>
  <form method="post">
	  <p>Дата:<br>
		  <input name="date" value="<?echo date('Y-m-d')?>" type="date">
	  </p>

    <p>Переменная:<br>
    <select name="variable">
    <option></option>
    <?
      for ($i = 0; $i < count($variables); $i ++)
      {
         $variables1 = $variables[$i]['name'];
         echo "<option>" . $variables1 . "</option>";
      }
    ?>
    </select>
    </p>

		<p>Значение:<br>
        <input name="value" type="text">
    </p>

		<input type="submit" name = "go"></input>
  </form>
<br>
<a href = 'index.php'>На главную</a>
</body>
</html>
<?
$adjustDate = $_POST['date'];
$adjustVariable = $_POST['variable'];
$adjustValue = $_POST['value'];

if(isset($_POST['go']) && !empty($adjustVariable) && !empty($adjustValue) && !empty($adjustDate))
{
    $queryAdjust = "INSERT INTO adjustments (date, variable, value)
                      VALUES ('$adjustDate', '$adjustVariable', '$adjustValue')";
    mysqli_query($db, $queryAdjust);
    echo "<script type='text/javascript'>window.location = 'adjustment.php'</script>";
}

?>
