<?

/* Данная функция, это большая часть кода с страницы salary.php, оптимизированный и доработанный для вывода начислений зарплаты и вывода выплат
для каждого сотрудника с разбивкой по месяцам */

function getSalary()
{
	$db = getConnect();// подключаемся к БД
	$query1 = "SELECT month(date), year(date) FROM income";//извлекаем все записи из БД за нужный период
	$sql1 = mysqli_query($db, $query1);
	while ($monthDate = mysqli_fetch_array($sql1))
	{
		$proverka = $monthDate['month(date)'];//переменная для того что бы проверить пусто ли в БД
		$month[] = $monthDate['month(date)'];
	}
	$month12 = array_values(array_unique($month));//Убираем повторяющиеся месяца и обнуляем ключ массива для корректного вывода значений

	if (isset($_GET['operation']))// проверяем есть ли GET['operation']
	{
		$month = $_GET['operation'];
	}
	else
	{
		$_GET['operation'] = date('m');// Если в GET ничего нет, то добавляем текущий месяц
		$month = $_GET['operation'];
	}

	$queryEmployees = "SELECT * FROM employees";//извлекаем все записи из БД
	$sqlEmployees = mysqli_query($db, $queryEmployees);
	while ($rowPos = mysqli_fetch_array($sqlEmployees))
	{
		$consul[] = $rowPos['name'];// добавляем в массив всех сотрудников из таблицы employees
	}

	if (!empty($proverka))// Если в БД есть записи, то выводим шапку таблицы
	{
		echo "<h3>Зарплата</h3>";
		echo "<table id='myTable1'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>Сотрудник</th>";
		echo "<th>Начислено</th>";
		echo "<th>Выплачено</th>";
		echo "</tr>";
		echo "</thead>";
	}

	for ($ee = 0; $ee < count($consul); $ee++)//перебираем всех сотрудников, и ниже подставляем каждого в код для расчета данных
	{
		//Обнуляем следующие массивы, чтобы при последующих итерациях были корректные значения
		$sumSal = array();
		$sumSal1= array();
		$ex = array();
		$ya = array();
		$paymants = array();
		$suma_viplat = array();
		$arSum = array();
		$arOr = array();
		$arOr1 = array();
		$mas = array();
		$allsumma = array();
		$allsummapercent = array();
		$premSum = array();
		$paymants1 = array();
		$work = array();
		$timeSum = array();
		$timeCharged = array();

		//получаем план продаж
		$cons1 = $consul[$ee];
		$querySales = "SELECT sum FROM sales_plan WHERE consultant = '$cons1' AND month(month) = '$month'";
		$sqlSales = mysqli_query($db, $querySales);
		$Sales = mysqli_fetch_array($sqlSales);
		$sumSales = $Sales['sum'];// это план продаж на выбранный месяц по сотруднику

		//получаем оклад сотрудник
		$queryEmployees = "SELECT salary FROM employees WHERE name = '$cons1'";
		$sqlEmployees = mysqli_query($db, $queryEmployees);
		$rowPos = mysqli_fetch_array($sqlEmployees);
		$oklad = $rowPos['salary'];// это оклад сотрудника

		//извлекаем все записи из БД за выбранный месяц отсортированные по дате
		$querySal = "SELECT * FROM income WHERE month(date)= '$month' ORDER BY date ASC";
		$sqlSal = mysqli_query($db, $querySal);
		while ($rowSal = mysqli_fetch_array($sqlSal))
		{
			$sumRowSal = $rowSal['order_schet'];//добавляем в переменную данные номер счета

			//извлекаем все данные из таблицы orders, где совпадают номера счетов и по определенному сотруднику
			$schekSumSal = "SELECT * FROM orders WHERE num = '$sumRowSal' AND consultant = '$cons1'";
			$schekOrderSal =  mysqli_query($db, $schekSumSal);
			while($oRowSal = mysqli_fetch_array($schekOrderSal))
			{
				$sumSal[] = $oRowSal['sum'];//добавляем в переменную данные сумму
			}

			//извлекаем все данные из таблицы orders, где совпадают номера счетов
			$schekSumSal1 = "SELECT * FROM orders WHERE num = '$sumRowSal'";
			$schekOrderSal1 =  mysqli_query($db, $schekSumSal1);
			while($oRowSal1 = mysqli_fetch_array($schekOrderSal1))
			{
				$sumSal1[] = $oRowSal1['sum'];//добавляем в массив сумму
				$ex[] = $oRowSal1['expense'];//добавляем в массив расходники
				$ya[] = $oRowSal1['yandex'];//добавляем в массив платежи по Яндексу
			}

		}

		$obSumSal = @array_sum($sumSal);//считаем общую сумму
		$obSumSal1 = @array_sum($sumSal1);//считаем общую сумму

		//извлекаем все данные из таблицы spending, за выбранный месяц по определенному сотруднику и где указана статья расходов - Зарплата
		$queryPayments = "SELECT * FROM spending WHERE recipient = '$cons1' AND month(date) = '$month' AND item = 'Зарплата'";
		$sqlPayments = mysqli_query($db, $queryPayments);
		while ($rowPay= mysqli_fetch_array($sqlPayments))
		{
			$paymants[] = $rowPay['sum'];//добавляем в массив все суммы выплат
		}
		$suma_viplat1 = @array_sum($paymants);//считаем общую сумму выплат

//--------------------------------Сейчас будет расчет для сотрудников у которых есть оклад, но не ставка(почасовая оплата)---------------------------------------------------------------------------------

		//извлекаем все записи из БД за выбранный месяц отсортированные по дате
		$query = "SELECT * FROM income WHERE month(date)= '$month' ORDER BY date ASC";
		$sql = mysqli_query($db, $query);
		while ($row = mysqli_fetch_array($sql))
		{
			$arSum[] = $row;//Добавляем в массив все данные за определённый месяц из таблицы income
			$sumRow = $row['order_schet'];//добавляем в переменную номер счета

			//извлекаем все данные из таблицы orders, где совпадают номера счетов и где сотруднику указан в стобце consultant
			$schekSum = "SELECT * FROM orders WHERE num = '$sumRow' AND consultant = '$cons1'";
			$schekOrder =  mysqli_query($db, $schekSum);
			while($oRow = mysqli_fetch_array($schekOrder))
			{
				$arOr[] = $oRow;//добавляем в массив все данные, где совпадают номера счетов
				$arOr1[] = $oRow['num'];//добавляем в массив номер счета
			}

			//извлекаем все данные из таблицы orders, где совпадают номера счетов и где сотруднику указан в стобце assistant
			$schekSum = "SELECT * FROM orders WHERE num = '$sumRow' AND assistant = '$cons1'";
			$schekOrder =  mysqli_query($db, $schekSum);
			while($oRow = mysqli_fetch_array($schekOrder))
			{
				$arOr[] = $oRow;//добавляем в массив все данные, где совпадают номера счетов
				$arOr1[] = $oRow['num'];//добавляем в массив номер счета
			}

			//извлекаем все данные из таблицы orders, где совпадают номера счетов и где сотруднику указан в стобце manager, consultant, assistant
			$schekSum = "SELECT * FROM orders WHERE num = '$sumRow' AND manager = '$cons1' AND consultant != '$cons1' AND assistant != '$cons1' ";
			$schekOrder =  mysqli_query($db, $schekSum);
			while($oRow = mysqli_fetch_array($schekOrder))
			{
				$arOr[] = $oRow;//добавляем в массив все данные, где совпадают номера счетов
				$arOr1[] = $oRow['num'];//добавляем в массив номер счета
			}

			if (!empty($proverka))//если date в массиве не пустая
			{
				$mas = array();//создаём массив в который будет занасится сумма, консультан, продукт за определённый счёт
				for ($i = 0; $i < count($arSum); $i ++)//перебираем массив с данными из таблицы income
				{
					$sr = $row['order_schet'];// помещаем в переменную номер счета из таблицы income
					$sumP = 0;
					for($j = 0; $j <count($arOr); $j ++)//перебираем массив с данными из таблицы orders
					{
						$or = $arOr[$j]['num'];// помещаем в переменную номер счета из таблицы orders
						if($sr == $or)// если номера счетов совпадают, то извлекаем информацию из orders и помещаем в переменные
						{
							$c = $arOr[$j]['consultant'];
							$a = $arOr[$j]['assistant'];
							$men = $arOr[$j]['manager'];
							$p = $arOr[$j]['product'];
							$percent = $arOr[$j]['assistant_percent'];
							$s = $arOr[$j]['sum'];
							$e = $arOr[$j]['expense'];
							$yan = $arOr[$j]['yandex'];
							// $e, $yan прибавляем для того чтобы, в таблице выводились записи (по условию запись выводится при равной сумме из income и orders)
							$sumP = $sumP + (int)$s + (int)$e + (int)$yan;
						}
					}

					if($sumP != 0)// если сумма не равна 0
					{
						$mas[$sr] = array("$sumP", $c, $p, $a, $men);//поместили в массив сумму, консультанта, продукт в котором ключь это номер счёта из таблицы income

					}

					$sr = $arSum[$i]['order_schet'];// помещаем в переменную номер счета из таблицы orders
					foreach ($mas as $key => $value)//перебираем массив с данными из таблицы $mas
					{
						if ($sr == $key)//если номера счетов совпадают
						{
							$t =  $arSum[$i]['sum'];
							if ($t == $value[0])//если суммы совпадают
							{
								for ($p = 0; $p < count($arOr); $p++)//перебираем массив с данными из таблицы $arOr
								{
									//если номера счетов совпадают и сумма не равна 0
									if (($row["order_schet"] == $arOr[$p]['num']) AND ($arOr[$p]['sum'] != 0))
									{
//Далее должны выводиться данные в таблицу(как на странице salary.php, но здесь нам это не нужно, поэтому опускаем этот код)

										$keyOrder = $key;
										$valueProduct = $arOr[$p]['num'];
										if ($keyOrder == $valueProduct)// если номера счетов совпадают
										{
											//условие определяющее выбор имени продукта
											if($arOr[$p]['product'] == null)
											{
												$productOrder = $arOr[$p]['appointment'];
											}
											else
											{
												$productOrder = $arOr[$p]['product'];
											}
										}

										$valueSumm = $arOr[$p]['num'];
										if ($keyOrder == $valueSumm)// если номера счетов совпадают
										{
											$summaOrder = $arOr[$p]['sum'];// помещаем в переменную сумму
											$allsumma[] = $arOr[$p]['sum'];// добавляем в массив сумму, для того чтобы потом посчитать итоговую сумму
										}

										//если консультант и менеджер равны определенному сотруднику, то считается процент
										if ($cons1 == $arOr[$p]['consultant'] && $cons1 == $arOr[$p]['manager'])
										{
											$percent1 = $percent/100;//ассистенский процент
											$queryProducts = "SELECT * FROM products WHERE product = '$productOrder'";
											$sqlProducts = mysqli_query($db, $queryProducts);
											if(mysqli_num_rows($sqlProducts) > 0)// если продукт совпадает с продуктом из таблицы Products
											{
												$queryAgency_percent = "SELECT * FROM products WHERE product = '$productOrder'";
												$sqlAgency_percent = mysqli_query($db, $queryAgency_percent);
												while ($rowAgency_percent = mysqli_fetch_array($sqlAgency_percent))
												{
													$z = $rowAgency_percent['agency_percent']/100;//агентский процент от продукта
													$man = $rowAgency_percent['manager_percent']/100;//менеджерский процент
													$obz = $rowAgency_percent['agency_percent']/100;//агентский процент от продукта
												}
												$z = $z - $percent1 + $man; // процент если у него есть ассистент и менеджер

												$allsummapercent[] = $summaOrder*$z;// добавляем процент в массив, для того чтобы потом посчитать итоговый процент
											}
											else // если продукт не совпадает с продуктом из таблицы Products
											{
												$obz = 0.1;//агентский процент если продукт не найден
												$man = 0;//менеджерский процент
												$z = 0.1 - $percent1 + $man;
												$allsummapercent[] = $summaOrder*$z;// добавляем процент в массив, для того чтобы потом посчитать итоговый процент
											}
											//если он выполнил план
											if ($sumSales != null)// Если план продаж есть, то считаем получает консультант премию или нет
											{
												if(($obSumSal > $sumSales) && ($obSumSal < ($sumSales + 50000)) && (!empty($sumSales)))
												{
													$pSum = $summaOrder * (0.15 - $obz);
													$premSum[] = $pSum;
												}
												//если он выполнил больше план, чем плана продаж + 50000
												elseif($obSumSal > ($sumSales + 50000) && (!empty($sumSales)))
												{
													$pSum = $summaOrder * (0.2 - $obz);
													$premSum[] = $pSum;
												}
											}
										}

										//если ассистент и менеджер равны определенному сотруднику, то счетается процент
										elseif ($cons1 == $arOr[$p]['assistant'] && $cons1 == $arOr[$p]['manager'])
										{
											$percent1 = $percent/100;
											$queryProducts = "SELECT * FROM products WHERE product = '$productOrder'";
											$sqlProducts = mysqli_query($db, $queryProducts);
											if(mysqli_num_rows($sqlProducts) > 0)// если продукт совпадает с продуктом из таблицы Products
											{
												$queryAgency_percent = "SELECT * FROM products WHERE product = '$productOrder'";
												$sqlAgency_percent = mysqli_query($db, $queryAgency_percent);
												while ($rowAgency_percent = mysqli_fetch_array($sqlAgency_percent))
												{
													$z = $rowAgency_percent['agency_percent']/100;//агентский процент от продукта
													$man = $rowAgency_percent['manager_percent']/100;//менеджерский процент
												}
												$z = $percent1 + $man;
												$allsummapercent[] = $summaOrder*$z;
											}
											else // если продукт не совпадает с продуктом из таблицы Products
											{
												$man = 0;
												$z = $percent1 + $man;
												$allsummapercent[] = $summaOrder*$z;
											}
											if ($sumSales != null)// Если план продаж есть, то считаем получает консультант премию или нет
											{
											//если объём продаж больше чем план продаж, то по этой позиции премию не получает
												if($obSumSal > $sumSales)
												{
												}
											}
										}

										//если консультант равен определенному сотруднику, то считается процент
										elseif ($cons1 == $arOr[$p]['consultant'])
										{
											$percent1 = $percent/100;
											$queryProducts = "SELECT * FROM products WHERE product = '$productOrder'";
											$sqlProducts = mysqli_query($db, $queryProducts);
											if(mysqli_num_rows($sqlProducts) > 0)// если продукт совпадает с продуктом из таблицы Products
											{
												$queryAgency_percent = "SELECT agency_percent FROM products WHERE product = '$productOrder'";
												$sqlAgency_percent = mysqli_query($db, $queryAgency_percent);
												while ($rowAgency_percent = mysqli_fetch_array($sqlAgency_percent))
												{
													$z = $rowAgency_percent['agency_percent']/100;//агентский процент от продукта
													$obz = $rowAgency_percent['agency_percent']/100;//агентский процент от продукта
												}
												$z = $z - $percent1;
												$allsummapercent[] = $summaOrder*$z;// добавляем процент в массив, для того чтобы потом посчитать итоговый процент
											}
											else // если продукт не совпадает с продуктом из таблицы Products
											{
												$obz = 0.1;
												$z = 0.1 - $percent1;
												$allsummapercent[] = $summaOrder*$z;// добавляем процент в массив, для того чтобы потом посчитать итоговый процент
											}
											if ($sumSales != null)// Если план продаж есть, то считаем получает консультант премию или нет
											{
												if(($obSumSal > $sumSales) && ($obSumSal < ($sumSales + 50000)) && (!empty($sumSales)))
												{
													$pSum = $summaOrder * (0.15 - $obz);
													$premSum[] = $pSum;
												}
												elseif($obSumSal > ($sumSales + 50000) && (!empty($sumSales)))
												{
													$pSum = $summaOrder * (0.2 - $obz);
													$premSum[] = $pSum;
												}
											}
										}

										//если ассистент равен определенному сотруднику, то считается процент
										elseif ($cons1 == $arOr[$p]['assistant'])
										{
											$percent1 = $percent/100;
											$queryProducts = "SELECT * FROM products WHERE product = '$productOrder'";
											$sqlProducts = mysqli_query($db, $queryProducts);
											if(mysqli_num_rows($sqlProducts) > 0)// если продукт совпадает с продуктом из таблицы Products
											{
												$queryAgency_percent = "SELECT agency_percent FROM products WHERE product = '$productOrder'";
												$sqlAgency_percent = mysqli_query($db, $queryAgency_percent);
												while ($rowAgency_percent = mysqli_fetch_array($sqlAgency_percent))
												{
													$z = $rowAgency_percent['agency_percent']/100;
												}
												$z = $percent1;
												$allsummapercent[] = $summaOrder*$z;

											}
											else // если продукт не совпадает с продуктом из таблицы Products
											{
												$z = $percent1;
												$allsummapercent[] = $summaOrder*$z;
											}
											if ($sumSales != null)// Если план продаж есть, то считаем получает консультант премию или нет
											{
												if($obSumSal > $sumSales)
												{
												}
											}
										}

										//если менеджер равен определенному сотруднику, то считается процент
										elseif($cons1 == $arOr[$p]['manager'])
										{
											$percent1 = $percent/100;
											$queryProducts = "SELECT * FROM products WHERE product = '$productOrder'";
											$sqlProducts = mysqli_query($db, $queryProducts);
											if(mysqli_num_rows($sqlProducts) > 0)// если продукт совпадает с продуктом из таблицы Products
											{
												$queryAgency_percent = "SELECT * FROM products WHERE product = '$productOrder'";
												$sqlAgency_percent = mysqli_query($db, $queryAgency_percent);
												while ($rowAgency_percent = mysqli_fetch_array($sqlAgency_percent))
												{
													$man = $rowAgency_percent['manager_percent']/100;
												}
												$allsummapercent[] = $summaOrder*$man;// добавляем процент в массив, для того чтобы потом посчитать итоговый процент
											}
											else // если продукт не совпадает с продуктом из таблицы Products
											{
												$man = 0;
												$z = 0.1 - $percent1;
												$allsummapercent[] = $summaOrder*$man;// добавляем процент в массив, для того чтобы потом посчитать итоговый процент
											}
											if ($sumSales != null)// Если план продаж есть, то считаем получает консультант премию или нет
											{
												if($obSumSal > $sumSales)
												{
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		//Если есть оклад или определенный сотрудник это Эдуард Вартанян или Георгий Слизинтуев, то производим расчеты
		if (!empty($oklad) || $cons1 == "Эдуард Вартанян" || $cons1 == "Георгий Слизинтуев")
		{
			$n = @array_sum($allsummapercent) + $oklad + @array_sum($premSum);// складываем оклад и проценты
			$premia_obshay = @array_sum($premSum);
			$premia_obshay1[] = $premia_obshay;
			if($cons1 == "Эдуард Вартанян" || $cons1 == "Георгий Слизинтуев")//если определенный сотрудник это Эдуард Вартанян или Георгий Слизинтуев
			{
				$procob = $obSumSal1*4/100;// считаем общий процент(зарплата руководителя + премия, каждая по 2%)
				$sumobshay = $procob + $n;// получаем общую сумму
				echo "<tr>";
				echo "<td class='th'>" . $cons1 . "</td>";// определенный сотрудник
				echo "<td class='th'>" . $sumobshay . "</td>";// сумма ничислений
				echo "<td class='th'>" . $suma_viplat1 . "</td>";// сумма выплат
				echo "</tr>";
			}
			else// для всех остальных сотрудников у кого есть оклад, но не ставка(почасовая оплата)
			{
				echo "<tr>";
				echo "<td class='th'>" . $cons1 . "</td>";// определенный сотрудник
				echo "<td class='th'>" . $n . "</td>";// сумма ничислений
				echo "<td class='th'>" . $suma_viplat1 . "</td>";// сумма выплат
				echo "</tr>";
			}
		}

//--------------------------------Сейчас будет расчет для сотрудников у ставка(почасовая оплата)---------------------------------------------------------------------------------

		//Если нет оклада и определенный сотрудник не Эдуард Вартанян или Георгий Слизинтуев, то производим расчеты
		if (($cons1 != "Эдуард Вартанян") AND ($cons1 != "Георгий Слизинтуев") AND (empty($oklad)))
		{
			$queryWatch = "SELECT * FROM complited_work WHERE specialist = '$cons1' AND month(date) = '$month'";//делаем выборку всех записей из БД
			$sqlWatch = mysqli_query($db, $queryWatch);
			while ($rowWatch = mysqli_fetch_array($sqlWatch))
			{
				$work[] =  $rowWatch;
			}
			$queryRate = "SELECT * FROM employees WHERE name = '$cons1'";//извлекаем все записи из БД
			$sqlRate = mysqli_query($db, $queryRate);
			while ($rowRate = mysqli_fetch_array($sqlRate))
			{
				$rate = $rowRate['rate'];// помещаем в переменную часовую ставку
			}

			for ($w = 0; $w < count($work); $w ++)
			{
				$task = $work[$w]['task'];//добавляем в переменную название продукта из таблице complited_work
				$time = $work[$w]['time'];//добавляем в переменную время из таблице complited_work
				$charged =	$rate / 60;//добавляем в переменную значение которое сотрудник получает за 1 мин, т.е. руб/мин,(например, 180р/ч делим на 60 мин = 3 р/мин)
				$time1 = explode(":", $work[$w]['time']);//добавляем в переменную отдельно часы и минуты
				$timeSum [] = $time1[0] * 60 + $time1[1];//добавляем в массив преобразованное всё время в минуты (например 3:40 => 3*60 + 40 = 220 минут)
				$timeCharged[] = ($time1[0] * 60 + $time1[1]) * $charged;// добавляем в массив сумму которую получил сотрудник за определённый продукт (например, 3р/мин * 220 мин = 660 рублей)
			}
			$timeSum = @array_sum($timeSum);//сумма всего времени по продуктам за месяц определённого сотрудника
			$timeSumInt = intval($timeSum/60);//делим всю сумму времени на 60, что бы выделить часы
			$ostatocTime = $timeSum % 60;//делим всю сумму времени и берём остаток
			if ($ostatocTime < 10)// если минуты меньше 10, то добавляем ноль в переди. Это для корректного отображения времени
			{
				$ostatocTime = "0" . $ostatocTime;//добавляем ноль(например, 2:5 => 2:05)
			}
			$timeAllSum = $timeSumInt . ":" . $ostatocTime;//соединяем 0 и число (например, 2:5 => 2:05)

			echo "<tr>";
			echo "<td class='th'>" . $cons1 . "</td>";// определенный сотрудник
			echo "<td class='th'>" . @array_sum($timeCharged) . "</td>";// сумма ничислений
			echo "<td class='th'>" . $suma_viplat1 . "</td>";// сумма выплат
			echo "</tr>";
		}
	}
	echo "</table>";
	global $premia_obshay2;
	$premia_obshay2 = @array_sum($premia_obshay1);
}

?>
