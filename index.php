<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="main.css">
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<meta name="robots" content="noindex,nofollow"/>
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		<title>Главная страница ERP mmit</title>
	</head>
	<body>
		<div>
			<a href="payments-import.php">Импорт выписки из банка</a>
		 </div>
		 <div>
			<a href="orders-import.php">Импорт счетов</a>
		  </div>
		<div>
			<a href="complited-work-import.php">Импорт выполненной работы</a>
		</div>
		<div>
			<a href="cash-income.php">Ввести оплаты наличными</a>
		</div>
		<div>
			<a href="add-spending.php">Ввести расходы</a>
		</div>
		<div>
			<a href="adjustment.php">Ввести корректировки</a>
		</div>
		<div>
			<a href="sources_adjunctions.php">Пополнение источников</a>
		</div>
		<div>
			<a href="add-contract.php">Добавить договор</a>
		</div>
		<div>
			<a href="change-salary.php">Изменить оклад</a>
		</div>
		<hr>
		<div>
			<?
			  $date = date('m');
			?>
			<a href="income.php">Оплаты</a>
		</div>
		<div>
			<a href="spending.php">Расходы</a>
		</div>
		<div>
			<a href="salary.php?operation=<?echo $date?>">Зарплата</a>
		</div>
		<div>
			<a href="finance-kpi.php">Ключевые финансовые показатели</a>
		</div>
		<hr>
		<div>
			<a href="customers-pays.php">Продажи по клиентам</a>
		</div>
		<div>
			<a href="source-pays.php">Продажи по источникам</a>
		</div>
		<div>
			<a href="product-pays.php">Продажи по продуктам</a>
		</div>
<?

include "functions-dump.php";
include "dbconnect.php";

$db = getConnect();

$productMas = array();// Объявляем $productMas массивом, для того чтобы ниже вывести данный в таблицу доход
$summaMas = array();// Объявляем $summaMas массивом, для того чтобы ниже вывести данный в таблицу доход

$querySpending = "SELECT * FROM spending";
$sqlSpending = mysqli_query($db, $querySpending);
while ($rowSpending = mysqli_fetch_array($sqlSpending))
{
	$Spending_item = $rowSpending["item"];
	$Spending_sum = $rowSpending["sum"];

	$queryCosts = "SELECT * FROM costs WHERE constant = 1 AND costs_name = '$Spending_item'";
	$sqlCosts = mysqli_query($db, $queryCosts);
	if(mysqli_num_rows($sqlCosts) > 0)
	{
		$Spending[] = $Spending_sum;
	}
}

$sumIp = @array_sum($sumIp);//сумма оплат по ИП
$sumOOO = @array_sum($sumOOO);//сумма оплат по ООО
$allsummaMas = @array_sum($summaMas);// считаем общую сумму, всех продуктов(она нужна для вычисления процентов)

//выбираем всем суммы из таблицы income
$querySum = "SELECT * FROM income";//делаем выборку всех записей из БД
$sqlSum = mysqli_query($db, $querySum);
while ($rowSum = mysqli_fetch_array($sqlSum))//пока есть записи о дате(месяце) в БД, добавляется в массив
{
    $account = $rowSum['account'];
    if ($account == 40802810818350019540){
        $sumRowIp[] = $rowSum['sum'];
    }
    elseif($account == 40702810818350002110){
        $sumRowOOO[] = $rowSum['sum'];
    }
    else
    {
        $sumRowCash[] = $rowSum['sum'];
    }
}

$allSumIp = @array_sum($sumRowIp);
$allSumOOO = @array_sum($sumRowOOO);
$allSumCash = @array_sum($sumRowCash);

//выбираем всем суммы из таблицы income
$querySumSpen = "SELECT * FROM spending";//делаем выборку всех записей из БД
$sqlSumSpen = mysqli_query($db, $querySumSpen);
while ($rowSumSpen = mysqli_fetch_array($sqlSumSpen))
{
	if ($rowSumSpen['source'] == 'Расходники')// если источник "Расходники", то добавляем в массив $ExpenseRas сумму, для вычисления показателя "Расходники"
	{
		$ExpenseRas[] = $rowSumSpen['sum'];
	}

    $accountSpen = $rowSumSpen['account'];
    $item = $rowSumSpen['item'];
    if ($accountSpen == "ИП")
    {
        $spenSumIp[] = $rowSumSpen['sum'];
    }
    elseif ($accountSpen == "ООО")
    {
        $spenSumOOO[] = $rowSumSpen['sum'];
    }
    else
    {
        $spenSumCash[] = $rowSumSpen['sum'];
    }

    if($accountSpen == "ИП" && $item == "Налог"){
        $sumTaxIp[] =  $rowSumSpen['sum'];
    }
    elseif($accountSpen == "ООО" && $item == "Налог"){
        $sumTaxOOO[] = $rowSumSpen['sum'];
    }
}

$allSumSpenIp = @array_sum($spenSumIp);
$allSumSpenOOO = @array_sum($spenSumOOO);
$allSumSpenCash = @array_sum($spenSumCash);
$allSumTaxIp = @array_sum($sumTaxIp);
$allSumTaxOOO = @array_sum($sumTaxOOO);


$balanceIp = $allSumIp - $allSumSpenIp;
$balanceOOO = $allSumOOO - $allSumSpenOOO;
$balanceCash = $allSumCash - $allSumSpenCash;

$queryBalanceIp = "UPDATE accounts SET balance = '$balanceIp' WHERE name = 'ИП'";
mysqli_query($db, $queryBalanceIp);
$queryBalanceOOO = "UPDATE accounts SET balance = '$balanceOOO' WHERE name = 'ООО'";
mysqli_query($db, $queryBalanceOOO);
$queryBalanceCash = "UPDATE accounts SET balance = '$balanceCash' WHERE name = 'Нал'";
mysqli_query($db, $queryBalanceCash);

$queryAccountBalance = "SELECT * FROM accounts";//делаем выборку всех записей из БД
$sqlAccountBalance = mysqli_query($db, $queryAccountBalance);
while ($rowBalance= mysqli_fetch_array($sqlAccountBalance))
{
    $balance[] = $rowBalance;
}

include "kpi-index.php";
$qwerty = getKFP();

echo "<h1>Баланс</h1>";
for($b = 0; $b < count($balance); $b ++){
    if ($balance[$b]['name'] == "ИП")
    {
        echo "ИП = " . $balance[$b]['balance'] . "<br>";
    }
    elseif($balance[$b]['name'] == "ООО")
    {
        echo "ООО = " . $balance[$b]['balance'] . "<br>";
    }
    else
    {
        echo "Наличные = " . $balance[$b]['balance'] . "<br>";
    }
}
$taxIp =  ($allSumIp * 0.06) - $allSumTaxIp;
$taxOOO = ($allSumOOO * 0.06) - $allSumTaxOOO;

//-------------------------------------МОЙ КОД--------------------------------------------------------------------------------------//
$queryExpense = "SELECT * FROM orders WHERE expense != 0";//делаем выборку всех записей из БД
$sqlExpense = mysqli_query($db, $queryExpense);
while ($rowExpense = mysqli_fetch_array($sqlExpense))
{
    $Expense[] = $rowExpense['expense'];// помещаем в массив $Expense сумму всех оплат на расходники
}

$Expense = @array_sum($Expense);// получаем сумму всех значений внутри массивов
$ExpenseRas = @array_sum($ExpenseRas);// получаем сумму всех значений внутри массивов
$Expense1 = $Expense - $ExpenseRas;// считаем показатель "Расходники"
echo "<br>Расходники = " . $Expense1;

$queryFond = "SELECT * FROM adjustments";//делаем выборку всех записей из БД
$sqlFond = mysqli_query($db, $queryFond);
while ($rowFond= mysqli_fetch_array($sqlFond))
{
	if ($rowFond['variable'] == 'Фонд развития')//если переменная равна Фонд развития, то сумму помещаем в массив $fond
	{
		$fond[] = $rowFond['value'];
	}
	elseif ($rowFond['variable'] == 'Инвестиции ВКА')//если переменная равна Инвестиции ВКА, то сумму помещаем в массив $invka
	{
		$invka[] = $rowFond['value'];
	}
	elseif ($rowFond['variable'] == 'Инвестиции НьюКорпорейшн')//если переменная равна Инвестиции НьюКорпорейшн, то сумму помещаем в массив $innk
	{
		$innk[] = $rowFond['value'];
	}
	elseif ($rowFond['variable'] == 'Призовой фонд отдела продаж')//если переменная равна Призовой фонд отдела продаж, то сумму помещаем в массив $prizfond
	{
		$prizfond[] = $rowFond['value'];
	}
	elseif ($rowFond['variable'] == 'Дивиденды ВКА')//если переменная равна Дивиденды ВКА, то сумму помещаем в массив $divvka
	{
		$divvka[] = $rowFond['value'];
	}
}
// получаем суммы всех значений внутри массивов
$fond = @array_sum($fond);
$invka = @array_sum($invka);
$innk = @array_sum($innk);
$prizfond = @array_sum($prizfond);
$divvka = @array_sum($divvka);

$querySpenFond = "SELECT * FROM spending";//делаем выборку всех записей из БД
$sqlSpenFond = mysqli_query($db, $querySpenFond);
while ($rowSpenFond = mysqli_fetch_array($sqlSpenFond))
{
	if ($rowSpenFond['source'] == 'Фонд развития')//если переменная равна Фонд развития, то сумму помещаем в массив $spenFond
	{
		$spenFond[] = $rowSpenFond['sum'];
	}
	elseif ($rowSpenFond['source'] == 'Инвестиции ВКА')//если переменная равна Инвестиции ВКА, то сумму помещаем в массив $spenInvka
	{
		$spenInvka[] = $rowSpenFond['sum'];
	}
	elseif ($rowSpenFond['source'] == 'Инвестиции НьюКорпорейшн')//если переменная равна Инвестиции НьюКорпорейшн, то сумму помещаем в массив $spenInnk
	{
		$spenInnk[] = $rowSpenFond['sum'];
	}
	elseif ($rowSpenFond['source'] == 'Призовой фонд отдела продаж')//если переменная равна Призовой фонд отдела продаж, то сумму помещаем в массив $spenPrizfond
	{
		$spenPrizfond[] = $rowSpenFond['sum'];
	}
	elseif ($rowSpenFond['source'] == 'Дивиденды ВКА')//если переменная равна Дивиденды ВКА, то сумму помещаем в массив $spenDivvka
	{
		$spenDivvka[] = $rowSpenFond['sum'];
	}
}
// получаем суммы всех значений внутри массивов
$spenFond = @array_sum($spenFond);
$spenInvka = @array_sum($spenInvka);
$spenInnk = @array_sum($spenInnk);
$spenPrizfond = @array_sum($spenPrizfond);
$spenDivvka = @array_sum($spenDivvka);

$querySourceFond = "SELECT * FROM sources_adjunctions";//делаем выборку всех записей из БД
$sqlSourceFond = mysqli_query($db, $querySourceFond);
while ($rowSourceFond= mysqli_fetch_array($sqlSourceFond))
{
	if ($rowSourceFond['source'] == 'Фонд развития')//если переменная равна Фонд развития, то сумму помещаем в массив $sourceFond
	{
		$sourceFond[] = $rowSourceFond['sum'];
	}
	elseif ($rowSourceFond['source'] == 'Инвестиции ВКА')//если переменная равна Инвестиции ВКА, то сумму помещаем в массив $sourceInvka
	{
		$sourceInvka[] = $rowSourceFond['sum'];
	}
	elseif ($rowSourceFond['source'] == 'Инвестиции НьюКорпорейшн')//если переменная равна Инвестиции НьюКорпорейшн, то сумму помещаем в массив $sourceInnk
	{
		$sourceInnk[] = $rowSourceFond['sum'];
	}
	elseif ($rowSourceFond['source'] == 'Призовой фонд отдела продаж')//если переменная равна Призовой фонд отдела продаж, то сумму помещаем в массив $sourcePrizfond
	{
		$sourcePrizfond[] = $rowSourceFond['sum'];
	}
	elseif ($rowSourceFond['source'] == 'Дивиденды ВКА')//если переменная равна Дивиденды ВКА, то сумму помещаем в массив $sourceDivvka
	{
		$sourceDivvka[] = $rowSourceFond['sum'];
	}
}

// получаем суммы всех значений внутри массивов
$sourceFond = @array_sum($sourceFond);
$sourceInvka = @array_sum($sourceInvka);
$sourceInnk = @array_sum($sourceInnk);
$sourcePrizfond = @array_sum($sourcePrizfond);
$sourceDivvka = @array_sum($sourceDivvka);
$pribkomp = $pribkomp * 0.25;
$fondP = $fond + $pribkomp - $spenFond + $sourceFond;// считаем Фонд развития
echo "<br>Фонд развития = " . $fondP;

$invka1 = $invka - $spenInvka + $sourceInvka;// считаем Инвестиции ВКА
echo "<br>Инвестиции ВКА = " . $invka1;

$innk1 = $innk - $spenInnk + $sourceInnk;// считаем Инвестиции НьюКорпорейшн
echo "<br>Инвестиции НьюКорпорейшн = " . $innk1;

$prizfond1 = $prizfond - $spenPrizfond + $sourcePrizfond;// считаем Призовой фонд отдела продаж
echo "<br>Призовой фонд отдела продаж = " . $prizfond1;

$divvka1 = $divvka - $spenDivvka + $sourceDivvka;// считаем Дивиденды ВКА
echo "<br>Дивиденды ВКА = " . $divvka1;
//-------------------------------МОЙ КОД----------------------------------------------------------------------------------//

echo "<h1>Налог к оплате</h1>";
echo "ИП = " . $taxIp . "<br>";
echo "ООО = " . $taxOOO . "<br>";
?>
    </body>
</html>
