<?php

function getKFP()
{
	$db = getConnect();
	$queryProductNew = "SELECT * FROM products ORDER BY product";
	$sqlProductNew = mysqli_query($db, $queryProductNew);
	while ($rowProductNew = mysqli_fetch_array($sqlProductNew))
	{
		$ProductNew[] = $rowProductNew;//получаем все продукты
	}

	$queryAdjustments = "SELECT * FROM adjustments WHERE variable = 'Премия оператору'";
	$sqlAdjustments = mysqli_query($db, $queryAdjustments);
	while ($rowAdjustments = mysqli_fetch_array($sqlAdjustments))
	{
		$Adjustments[] = $rowAdjustments["value"];//получаем значения для Премия оператору
	}

	$queryAdjustments1 = "SELECT * FROM adjustments WHERE variable = 'Премия Скуматову'";
	$sqlAdjustments1 = mysqli_query($db, $queryAdjustments1);
	while ($rowAdjustments1 = mysqli_fetch_array($sqlAdjustments1))
	{
		$Adjustments1[] = $rowAdjustments1["value"];//получаем значения для Премия Скуматову
	}

	$Employees_employe = array();// Объявляем $Employees_employe массивом, для того чтобы ниже посчитать актуальные оклады
	$Employees_data = array();// Объявляем $Employees_employe массивом, для того чтобы ниже посчитать актуальные оклады
	$Employees_salary = array();// Объявляем $Employees_employe массивом, для того чтобы ниже посчитать актуальные оклады

	$query1 = "SELECT month(date) FROM income ORDER BY date ASC";//извлекаем все записи из БД за нужный период
	$sql1 = mysqli_query($db, $query1);
	while ($monthDate = mysqli_fetch_assoc($sql1))
	{
		$month2[] = $monthDate['month(date)'];
	}
	$month1 = array_values(array_unique($month2));//Убираем повторяющиеся месяца и обнуляем ключ массива для корректного вывода значений

	for ($rf = 0; $rf < count($month1); $rf++)//перебираем все месяца
	{
		$month = $month1[$rf];
		$queryEmployees = "SELECT * FROM employees_changes WHERE month(date) <= '$month'";//извлекаем все записи из БД
		$sqlEmployees = mysqli_query($db, $queryEmployees);
		while ($rowEmployees = mysqli_fetch_array($sqlEmployees))
		{
			$employe = $rowEmployees["employe"];// помещаем в переменную $employe id сотрудника
			if(in_array("$employe", $Employees_employe))// Если в массиве $Employees_employe есть значение $employe, то выполняем следующее
			{
				$key1 = array_search("$employe", $Employees_employe);//получаем ключ, в котором такой же $employe
				$data1 = strtotime($Employees_data[$key1]);// берем дату из $Employees_data с таким же ключом как и у $employe и приводим ее к виду strtotime
				$data2 = strtotime($rowEmployees["date"]);//извлекаем дату из БД и приводим ее к виду strtotime

				if ($data1 > $data2)// Если дата из $Employees_data актуальнее даты из БД, то ничего не делаем
				{
				}
				else// Если дата из БД актуальнее даты из $Employees_data, то заменяем оклад в $Employees_salary на актуальный
				{
					$Employees_salary[$key1] = $rowEmployees["salary"];
					$Employees_data[$key1] = $rowEmployees["date"];
				}
			}
			else// Если в массиве $Employees_employe нет значение $employe, то добавляем значения в массивы
			{
				$Employees_employe[] = $rowEmployees["employe"];
				$Employees_data[] = $rowEmployees["date"];
				$Employees_salary[] = $rowEmployees["salary"];
			}
		}
		$Employees = @array_sum($Employees_salary);// считаем актуальные оклады сотрудников
		$Employees1[] = $Employees;
	}
	$Employees2 = @array_sum($Employees1);

	$querySpending = "SELECT * FROM spending";
	$sqlSpending = mysqli_query($db, $querySpending);
	while ($rowSpending = mysqli_fetch_array($sqlSpending))
	{
		$Spending_item = $rowSpending["item"];
		$Spending_sum = $rowSpending["sum"];
		$queryCosts = "SELECT * FROM costs WHERE constant = 1 AND costs_name = '$Spending_item'";
		$sqlCosts = mysqli_query($db, $queryCosts);
		if(mysqli_num_rows($sqlCosts) > 0)
		{
			$Spending[] = $Spending_sum;
		}
	}

	$queryIncome = "SELECT * FROM income";//извлекаем записи из БД
	$sqlIncome = mysqli_query($db, $queryIncome);
	while ($rowIncome = mysqli_fetch_assoc($sqlIncome))
	{
		$Income[] = $rowIncome;
		$recipient = $rowIncome['account'];
		if ($recipient == 40802810818350019540)
		{
			$sumIp[] = $rowIncome['sum'];//добавляем в массив сумму оплат по ИП
		}
		elseif ($recipient == 40702810818350002110)
		{
			$sumOOO[] = $rowIncome['sum'];//добавляем в массив сумму оплат по ООО
		}	
	}

	$queryOrders = "SELECT * FROM orders";//извлекаем все записи из БД
	$sqlOrders = mysqli_query($db, $queryOrders);
	while ($rowOrders = mysqli_fetch_assoc($sqlOrders))
	{
		$Orders[] = $rowOrders;
	}

	for ($dc = 0; $dc < count($Income); $dc++)// перебираем все данные в массиве $Income1 за определенный месяц
	{
		$sumRow = $Income[$dc]["order_schet"];// помещаем в переменную номер счета
		for ($xz = 0; $xz < count($Orders); $xz++)//перебираем все данные массива $Orders
		{
			if ($Orders[$xz]["num"] == $sumRow)// если номера счетов совпадают, то
			{
				$Orders1[] = $Orders[$xz];// помещаем в массив $Orders1 вложенные массивы, с данными по месяцам
				$ya[] = $Orders[$xz]["yandex"];
			}
		}
	}				

	/*----- /КОД ОТВЕЧАЮЩИЙ ЗА ОТОБРАЖЕНИЕ ТАБЛИЦЫ "ДОХОД" + отображения Отработанных часов по обслуживанию -----*/
	$productMas = array();// Объявляем $productMas массивом, для того чтобы ниже вывести данный в таблицу доход
	$summaMas = array();// Объявляем $summaMas массивом, для того чтобы ниже вывести данный в таблицу доход

	if ($month1 != null)//если date в массиве не пустая
	{
		$masAllsummOrders = array();//создаём массив в который будет занасится сумма, консультан, продукт за определённый счёт
		for ($i = 0; $i < count($Income); $i ++)
		{	
			$sr = $Income[$i]['order_schet'];
			$sumP = 0;
			for($j = 0; $j <count($Orders1); $j ++)
			{
				$or = $Orders1[$j]['num'];
				if($sr == $or)
				{
					$c = $Orders1[$j]['consultant'];
					$p = $Orders1[$j]['product'];
					$s = $Orders1[$j]['sum'];
					$e = $Orders1[$j]['expense'];
					$yan = $Orders1[$j]['yandex'];
					$sumP = $sumP + (int)$s + (int)$e + (int)$yan;// $e прибавляем для того чтобы, в таблице выводились записи (т.к. у нас условие что запись выводится при равной сумме из income и orders)
				}
			}
			if($sumP != 0)
			{
				$masAllsummOrders[$sr] = array("$sumP", $c, $p);//поместили в массив сумму, консультанта, продукт в котором ключь это номер счёта из таблицы income
			}
			$sr = $Income[$i]['order_schet'];
			foreach ($masAllsummOrders as $key => $value)
			{
				if ($sr == $key)
				{
					$t =  $Income[$i]['sum'];
					if ($t == $value[0])
					{
						for ($p = 0; $p < count($Orders1); $p++)
						{
							if ($Income[$i]["order_schet"] == $Orders1[$p]['num'])
							{
								$keyOrder = $key;//берём счет у которого суммы совпали
								$valueNum = $Orders1[$p]['num'];//счёт из ордерс
								if ($keyOrder == $valueNum)
								{
									if($Orders1[$p]['product'] == null)
									{
										$productOrder = $Orders1[$p]['appointment'];//добавляем дефолтный продукт если обновлённый продукт пуст
									}
									else{
										$productOrder = $Orders1[$p]['product'];
									}

									$appointment = $Orders1[$p]['appointment'];

									$summaMas1 = $Orders1[$p]['sum'];//добавляем в переменную сумму

									if(preg_match("(Обслуживание сайта)", "$productOrder"))
									{
										$summaObsl[] = $summaMas1;//добавляем в массив сумму продукта "Обслуживание сайта"
									}
									else
									{
										if(in_array("$productOrder", $productMas))// Если в массиве $productMas есть значение $productOrder, то выполняем следующее
										{
											$key1 = array_search("$productOrder", $productMas);// // получаем ключ, в котором такой же продукт
											$obs = $summaMas[$key1] + $summaMas1;// складываем суммы одинаковых продуктов
											$summaMas[$key1] = $obs;// присваиваем существующему продукту сумму одинаковых продуктов
										}
										else// если в массиве такого значения нет, то добавляем значения в массивы
										{
											$productMas[] = $productOrder;//добавляем в массив названия продуктов
											$summaMas[] = $summaMas1;//добавляем в массив сумму для каждого продукта
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	$percentAll = array();//объявляем массив в котором будут хранится сумма процентов по каджому продукту
	for($p = 0; $p < count($ProductNew); $p++)
	{
		if(!preg_match("(Обслуживание сайта)", $ProductNew[$p]['product']))//если в продукте не найдено словосочитание "Обслуживание сайта", то добавляем в массив сумму всех процентов
		{
			$percentAll[$ProductNew[$p]["product"]] =  (int)$ProductNew[$p]["agency_percent"] +  (int)$ProductNew[$p]["manager_percent"]
			+ (int)$ProductNew[$p]["chief_percent"] + (int)$ProductNew[$p]["marketing_percent"] + (int)$ProductNew[$p]["specialist_percent"] + (int)$ProductNew[$p]["expense_percent"];
		}
		else{
				//добавляем сумму всех процентов, где в продукте есть "Обслуживание сайта"
			$percentAllS =  (int)$ProductNew[$p]["agency_percent"] +  (int)$ProductNew[$p]["manager_percent"]
			+ (int)$ProductNew[$p]["chief_percent"] + (int)$ProductNew[$p]["marketing_percent"] + (int)$ProductNew[$p]["specialist_percent"] + (int)$ProductNew[$p]["expense_percent"];
		}
	}
	$percentAll['Обслуживание сайта'] = $percentAllS;//добавляем продукт "Обслуживание сайта" и его сумму процентов.
	$summaObsl123 = @array_sum($summaObsl);//сумма продукта "Обслуживание сайта"

	$productMas[] = "Обслуживание сайта";//добавляем продукт "Обслуживание сайта" ко всем продуктам
	$summaMas[] = $summaObsl123;//добавляем сумму к продукту "Обслуживание сайта"
	$sumIp = @array_sum($sumIp);//сумма оплат по ИП
	$sumOOO = @array_sum($sumOOO);//сумма оплат по ООО

	include "salary-index.php";
	$qwerty2 = getSal();
	
	$allsummaMas = @array_sum($summaMas);// считаем общую сумму, всех продуктов(она нужна для вычисления процентов)
	foreach ($percentAll as $key => $value)
	{
		for($i = 0; $i < count($productMas); $i ++)// перебираем массивы, вытягивая значения и выводя их в таблицу
		{
			if($key == $productMas[$i])
			{
				$productMas2 = $productMas[$i];
				$summaMas2 = $summaMas[$i];
				$procent = $summaMas2*100/$allsummaMas;//формула такая: сумму продукта умножаем на 100% и делим на общую сумму
				$procent2[] = $procent;
				$prib = $summaMas2*((100 - $value - 4)/100); //расчёт прибыли: сумма продукта * на (100% - процент по продукту - 4%)
				$pribil[] = $prib;
			}
		}
	}
	/*----- /КОД ОТВЕЧАЮЩИЙ ЗА ОТОБРАЖЕНИЕ ТАБЛИЦЫ "ДОХОД" + отображения Отработанных часов по обслуживанию -----*/

	$pribil = @array_sum($pribil);//сумма прибыли
	$procent2 = @array_sum($procent2);
	$Adjustments = @array_sum($Adjustments);
	$Adjustments1 = @array_sum($Adjustments1);
	$Spending1 = @array_sum($Spending);
	$premiaruk = $allsummaMas * 4/100;
	$nalogDohod = ($sumIp + $sumOOO - array_sum($ya)) * 0.06;//“Налог с доходов” - сумма оплат по ИП, умноженная на 6%.
	$kommisaBanka = ($sumIp + $sumOOO) * 0.015;//“Налог с доходов” - сумма оплат по ИП, умноженная на 1,5%.
	$pribilProduct = $pribil - $nalogDohod - $kommisaBanka;//“Прибыль с продуктов” = Сумма по столбцу Прибыль - Налог с доходов ИП - Комиссия банка.
	global $pribkomp;
	$pribkomp = $pribilProduct - $premia_obshay1 - $Adjustments - $Adjustments1 - $Employees2 - $premiaruk - $Spending1;//"Прибыль компании" = Прибыль с продуктов - Бонусы консультантам - Бонус оператор - Бонус Скуматов - Оклады - Премия руководителям - Постоянные расходы
}
?>

