<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Расходы</title>
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
    <table id="myTable">
<?
include "functions-dump.php";// подключаем функцию dump
include "dbconnect.php";// подключаем функцию getConnect с параметрами подключения к БД

$db = getConnect();// подключаемся к БД
$querySpenDate = "SELECT month(date) FROM spending";//извлекаем все записи из БД за нужный месяц
$sqlSpenDate = mysqli_query($db, $querySpenDate);
while ($monthDate = mysqli_fetch_array($sqlSpenDate))
{
	$proverka = $monthDate['month(date)'];//переменная для того что бы проверить пусто ли в БД
	$month[] = $monthDate['month(date)'];
}
@$monthSpen = array_values(array_unique($month));//Убираем повторяющиеся месяца и обнуляем ключ массива для корректного вывода значений
if (isset($_GET['operation']))// проверяем есть ли GET['operation']
{
	$month = $_GET['operation'];
}
else
{
	$_GET['operation'] = date('m');// Если в GET ничего нет, то добавляем текущий месяц
	$month = $_GET['operation'];
}

$months = array( 1 =>
                    'Январь' ,
                    'Февраль' ,
                    'Март' ,
                    'Апрель' ,
                    'Май' ,
                    'Июнь' ,
                    'Июль' ,
                    'Август' ,
                    'Сентябрь' ,
                    'Октябрь' ,
                    'Ноябрь' ,
                    'Декабрь'
                );// массив с русскими названиями месяцев

for($m = 1;$m <= 12; $m ++)
{
	for($k=0; $k < count($monthSpen); $k++)
	{
		if($monthSpen[$k] == $m)
		{
			$months_date = date($months[date($m)]);// данный блок кода нужен для того, чтобы выбранный месяц выделялся жирным шрифтом
			if ($month != $m)
			{
				$month1 = "<a href='?operation=" . $m ."'>" . $months_date . "</a>";
				echo $month1;
			}
			else
			{
				$month1 = "<a id='videleno' href='?operation=" . $m ."'>" . $months_date . "</a>";
				echo $month1;

			}

		}
	}
}
echo "<br><br>";
if (!empty($proverka))// Если в БД есть записи, то выводим шапку таблицы
{
	echo "<thead>";
    	echo "<tr>";
        	echo "<th>Дата</th>";
        	echo "<th>Получатель</th>";
        	echo "<th>Статья</th>";
        	echo "<th>Счёт</th>";
			echo "<th>Источник</th>";
        	echo "<th>Сумма</th>";
    	echo "</tr>";
	echo "</thead>";
}

$queryAllSpen = "SELECT * FROM spending WHERE month(date)= '$month' ORDER BY date ASC";//извлекаем все записи из БД за нужный месяц отсортированные по дате
$sqlAllSpen = mysqli_query($db, $queryAllSpen);
while ($rowSpen = mysqli_fetch_array($sqlAllSpen))
{
    $SpenRow[] = $rowSpen;
}
for($i = 0; $i < count($SpenRow); $i ++)// перебираем массив $SpenRow, чтобы вывести нужные нам данные
{
    $dateSpen = $SpenRow[$i]['date'];
    $recipientSpen = $SpenRow[$i]['recipient'];
    $itemSpen = $SpenRow[$i]['item'];
    $accountSpen = $SpenRow[$i]['account'];
    $sumSpen = $SpenRow[$i]['sum'];
	$sourceSpen = $SpenRow[$i]['source'];
	$allsum[] = $SpenRow[$i]['sum'];
    echo "<tr>";
	echo "<td class='th'>" . $dateSpen . "</td>";
	echo "<td class='th'>" . $recipientSpen . "</td>";
	echo "<td class='th'>" . $itemSpen . "</td>";
	echo "<td class='th'>" . $accountSpen . "</td>";
	echo "<td class='th'>" . $sourceSpen . "</td>";
	echo "<td class='th'>" . $sumSpen . "</td>";
    echo "</tr>";
}

//делаем проверку на то что переменная $proverka не пустая для вывода объёма продаж
if (!empty($proverka))
{
	echo "<thead>"; //оформляем последнюю строку таблицы в теги "<thead>" для того чтобы эта строка не поддавалась сортировке
	echo "<tr>";
	echo "<td class='th' colspan='5'>Итого:</td>";
	echo "<td class='th'>" . @array_sum($allsum) . "</td>";
	echo "</tr>";
}
?>
</table>
<br>
<a href = 'index.php'>На главную</a>
<hr>
<h3>Общий расход</h3>
<table>
<?
if (!empty($proverka))// Если в БД есть записи, то выводим шапку таблицы
{
	echo "<thead>";
		echo "<tr>";
			echo "<th>Статья</th>";
			echo "<th>Сумма</th>";
		echo "</tr>";
	echo "</thead>";
}
while ($rowSpen = mysqli_fetch_array($sqlAllSpen))
{
    $SpenRow[] = $rowSpen;//извлекаем все записи из БД за нужный месяц отсортированные по дате и помещаем в массив $SpenRow
}
$itemSpen = array();//объявляем $itemSpen массивом
$sumSpen = array(); //объявляем $sumSpen массивом

for($i = 0; $i < count($SpenRow); $i ++)// перебираем массив $SpenRow, чтобы вывести нужные нам данные
{
	$itemSpen1 = $SpenRow[$i]['item'];
	$sumSpen1 = $SpenRow[$i]['sum'];

	if(in_array("$itemSpen1", $itemSpen))// Если в массиве $itemSpen есть такое значение($itemSpen1), то выполняем следующее
	{
		$key = array_search("$itemSpen1", $itemSpen);// получаем ключ, в котором такая же статья
		$obs = $sumSpen[$key] + $sumSpen1;// складываем значения одинаковых статьи
		$sumSpen[$key] = $obs;// присваиваем существующей статье сумму одинаковых статей

	}
	else// если в массиве такого значения нет, то добавляем значения в массивы
	{
		$itemSpen[] = $itemSpen1;
		$sumSpen[] = $sumSpen1;
	}
}

for($i = 0; $i < count($itemSpen); $i ++)// перебираем массивы, извлекая значения и выводя их в таблицу
{
	$itemSpen2 = $itemSpen[$i];
    $sumSpen2 = $sumSpen[$i];
	echo "<tr>";
		echo "<td class='th'>" . $itemSpen2 . "</td>";
		echo "<td class='th'>" . $sumSpen2 . "</td>";
    echo "</tr>";
}
?>
</table>
</body>
</html>
