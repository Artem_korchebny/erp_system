<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Ввести оплаты наличными</title>
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	</head>
	<script>
//скрипт для скрытия/открытия формы
function disp(form) {
    if (form.style.display == "none") {
        form.style.display = "block";
    } else {
        form.style.display = "none";
    }
}
	</script>
	<body>
		<div>
			<form action="" method="post">
<?
	$name_all = array();//создаем массив для сбора в него имен плательщиков(чтобы исключить повторяющие)
	function getConnect()//подключение к БД
	{
		$db = mysqli_connect("localhost", "u0264176_erp", "vo6F3azO", "u0264176_erp");
		mysqli_query($db, "SET NAMES utf8");
		return $db;
	}
    $db = getConnect();
    $queryCust = "SELECT * FROM customers ORDER BY commercial_name";//делаем выборку всех записей из БД
    $sqlCust = mysqli_query($db, $queryCust);
    while ($rowCust = mysqli_fetch_array($sqlCust)) 
	{
		$cust[] = $rowCust;// добавляем все данные из БД в один массив для удобства
    }
    $queryProducts = "SELECT * FROM products ORDER BY product";//делаем выборку всех записей из БД
    $sqlProducts = mysqli_query($db, $queryProducts);
    while ($rowProducts = mysqli_fetch_array($sqlProducts)) 
	{
		$products[] = $rowProducts;// добавляем все данные из БД в один массив для удобства
    }
    $queryEmployees = "SELECT * FROM employees";//делаем выборку всех записей из БД
    $sqlEmployees = mysqli_query($db, $queryEmployees);
    while ($rowEmployees = mysqli_fetch_array($sqlEmployees)) 
	{
		$employees[] = $rowEmployees;// добавляем все данные из БД в один массив для удобства
    }
?>
			<p>Дата:<br>
			<input type="date" name="date"></input>
			</p>
			
			<p>Плательщик:<br>
			<select name="payer">
			<option></option>
            <?
			
			for ($w = 0; $w < count($cust); $w ++)// перебираем массив $cust
			{
				if($cust[$w]["commercial_name"] != null)// если есть коммерческое имя, то берем его
				{
					$name = $cust[$w]["commercial_name"];
				}
				else// если коммерческого имени нет, берем юридическое имя
				{
					$name = $cust[$w]["legal_name"];
				}
				if(in_array("$name", $name_all))// проверяем есть ли в массиве такое имя, если есть то ничего не делаем
				{
				}
				else// если в массиве такого значения нет, то добавляем значения в массив
				{
					$name_all[] = $name;
				}
			}
				
			for ($i = 0; $i < count($name_all); $i ++)// перебираем массив и выводим плательщиков
			{
				$Name1 = $name_all[$i];
				echo "<option>" . $Name1 . "</option>";
			}
            ?>
			</select>
			</p>

          	<div id="test" style="display:none;">Введите данные плательщика<br>
				  Введите номер ИНН:
				  <br><input name='innCustomers' type='text'><br>
				  Введите юридическое имя:
				  <br><input name='legal_nameCustomers' type='text'><br>
				  Введите коммерческое имя:
				  <br><input name='commercial_nameCustomers' type='text'><br>
				  <input name="formCustomers" type='submit'></input><br>
            </div>
            <a onclick="disp(document.getElementById('test'))">Добавить нового плательщика</a>
              <?
              	$innCustomers = $_POST['innCustomers'];
              	$legal_nameCustomers = $_POST['legal_nameCustomers'];
              	$commercial_nameCustomers = $_POST['commercial_nameCustomers'];
                if(isset($_POST['formCustomers']) && !empty($innCustomers) && !empty($legal_nameCustomers) && !empty($commercial_nameCustomers))
              	{
              		$db = mysqli_connect("localhost", "u0264176_erp", "vo6F3azO", "u0264176_erp");
              		mysqli_query($db, "SET NAMES utf8");
              		$sqlinnCustomers = "SELECT * FROM customers";
              		$qwerryinnCustomers = mysqli_query($db, $sqlinnCustomers);
              		while($rowInn = mysqli_fetch_array($qwerryinnCustomers))
              		{
              			$inn[] = trim($rowInn['inn']);
              		}
              		if (@!in_array($innCustomers, $inn))
              		{
                    $queryCustomers = "INSERT INTO customers (inn, legal_name, commercial_name, new) VALUES ('$innCustomers', '$legal_nameCustomers', '$commercial_nameCustomers', '1')";
              			$mysqliCustomers = mysqli_query($db, $queryCustomers);
              		}
					echo "<script type='text/javascript'>window.location = 'cash-income.php'</script>";
              	}
              ?>

			<p>Первая оплата:<br>
			<input name="first" type="checkbox">
			</p>

			<p>Номер счёта:<br>
			<input name="num" type="text">
			</p>

			<p>Продукт:<br>
			<select name="product">
			  <option></option>
				<?
				  for ($i = 0; $i < count($products); $i ++)
				  {
					$product = $products[$i]['product'];
					echo "<option>" . $product . "</option>";
				  }
				?>
			</select>
			</p>
            <div id="test1" style="display: none;">Введите новый продукт<br>
            Продукт:
            <br><input name='productNew' type='text'><br>
            Агентский процент:
            <br><input name='agency_percent' type='text'><br>
            <input name="formProduct" type='submit'></input><br>
            </div>
            <a onclick="disp(document.getElementById('test1'))">Добавить новый продукт</a>
            <?
            	$Product = $_POST['productNew'];
            	$Agency_percent = $_POST['agency_percent'];
            	if(isset($_POST['formProduct']) && !empty($Product) && !empty($Agency_percent))
            	{
            		$db = mysqli_connect("localhost", "u0264176_erp", "vo6F3azO", "u0264176_erp");
            		mysqli_query($db, "SET NAMES utf8");
            		$sqlProducts = "SELECT * FROM products";
            		$qwerryProducts = mysqli_query($db, $sqlProducts);
            		while($rowProducts = mysqli_fetch_array($qwerryProducts))
            		{
            			$ProductMas[] = trim($rowProducts['product']);
            		}
            		if (@!in_array($Product, $ProductMas))
            		{
						$queryProduct = "INSERT INTO products (product, agency_percent) VALUES ('$Product', '$Agency_percent')";
            			$mysqliProduct = mysqli_query($db, $queryProduct);
            		}
					echo "<script type='text/javascript'>window.location = 'cash-income.php'</script>";
            	}
            ?>

			<p>Консультант:<br>
			<select name="consultant">
			  <option></option>
				<?
					for ($i = 0; $i < count($employees); $i ++)
					{
						$consultant = $employees[$i]['name'];
						echo "<option>" . $consultant . "</option>";
					}
				?>
			</select>
			</p>

            <div id="test3" style="display: none;"  >Добавьте нового сотрудника<br>
            Имя и фамилия:
            <br><input name='fio' type='text'><br>
            Должность:
            <br><input name='position' type='text'><br>
            <input name="formEmployees" type='submit'></input><br>
            </div>
            <a onclick="disp(document.getElementById('test3'))">Добавить нового сотрудника</a>
            <?
            	$fio = $_POST['fio'];
            	$position = $_POST['position'];
            	if(isset($_POST['formEmployees']) && !empty($fio) && !empty($position))
            	{
            		$db = mysqli_connect("localhost", "u0264176_erp", "vo6F3azO", "u0264176_erp");
            		mysqli_query($db, "SET NAMES utf8");
            		$sqlEmployees = "SELECT * FROM employees";
            		$qwerryEmployees = mysqli_query($db, $sqlEmployees);
            		while($rowEmployees = mysqli_fetch_array($qwerryEmployees))
            		{
            			$EmployeesMas[] = trim($rowEmployees['name']);
            		}
            		if (@!in_array($fio, $EmployeesMas))
            		{
						$queryEmployees = "INSERT INTO employees (name, position) VALUES ('$fio', '$position')";
            			$mysqliEmployees = mysqli_query($db, $queryEmployees);
            		}
					echo "<script type='text/javascript'>window.location = 'cash-income.php'</script>";
            	}
            ?>

			<p>Сумма:<br>
			<input name="summ" type="text">
			</p>

			<p>Расходники:<br>
			<input name="expense" type="text">
			</p>

			<p>Яндекс:<br>
			<input name="yandex" type="text">
			</p>
		   
			<input type="submit" name = "go"></input>
			</form>
			<p><a href = 'index.php'>На главную</a></p>
		</div>
	</body>
</html>
<?
$dateCash = $_POST['date'];
$orderCash = rand(0, 1000);
$commentCash = "Оплата наличными";
$accountCash = "Наличные";
$payerCash = $_POST['payer'];
$firstCash = $_POST['first'];
//если галочка стоит добавляет в переменную 1
if ($firstCash == "on")
{
	$firstCash = "1";
}
else
{
	$firstCash = "0";
}
$numCash = $_POST['num'];
$productCash = $_POST['product'];
$consulCash = $_POST['consultant'];
$summCash = $_POST['summ'];
$expenseCash = $_POST['expense'];
$yandexCash = $_POST['yandex'];
//если кнопка нажата
if(isset($_POST['go']) && !empty($dateCash) && !empty($payerCash) && !empty($numCash) && !empty($productCash) && !empty($consulCash) && !empty($summCash))
{
	$db = getConnect();
	$queryInn = "SELECT * FROM customers";//делаем выборку всех записей из БД
	$sqlInn = mysqli_query($db, $queryInn);
	while($rowInn = mysqli_fetch_array($sqlInn))
	{
		$inn[] = $rowInn;
		for ($j = 0; $j < count($inn); $j ++)
		{
			$comInn = trim($inn[$j]['commercial_name']);
			$legInn =  trim($inn[$j]['legal_name']);
			if ($legInn == $payerCash || $comInn == $payerCash)
			{
				$innCash = $inn[$j]['inn'];
			}
		}
	}
	//добавление в таблицу orders
	$summCashS = (int)$summCash - ((int)$expenseCash + (int)$yandexCash);

	$queryNum = "SELECT * FROM orders WHERE num ='$numCash' AND product = '$productCash'";
	$sql1 = mysqli_query($db, $queryNum);
	if(mysqli_num_rows($sql1) <= 0)
	{
		$deleteArValue = "DELETE FROM orders WHERE num ='$numCash' AND product != '$productCash'";
		mysqli_query($db, $deleteArValue);
	}

	$queryNum1 = "SELECT * FROM orders WHERE num ='$numCash' AND product = '$productCash'";
	$sql = mysqli_query($db, $queryNum1);
	if(mysqli_num_rows($sql) <= 0)
	{
		$queryOrCash = "INSERT INTO orders (num, consultant, product, sum, expense, yandex, first_pay, customers)
		VALUES ('$numCash', '$consulCash', '$productCash', '$summCashS', '$expenseCash', '$yandexCash', '$firstCash', '$payerCash')";
		mysqli_query($db, $queryOrCash);
	}

	//добавление в таблицу income
	$resProv = "SELECT * FROM `income` WHERE `date`='$dateCash' AND order_num = '$orderCash'
	AND `sum`='$summCash' AND `payer_inn`='$innCash'";
	$resUnic = mysqli_query($db, $resProv);

	//выбираем Счета для проверки на уникальность
	$resOrder = "SELECT order_schet FROM income WHERE order_schet = '$numCash'";
	$resOr = mysqli_query($db, $resOrder);

	//если есть схожие счета, добавляем в конец счёта "Дубль"
	if (mysqli_num_rows($resOr) > 0)
	{
		$numCash = $numCash . "-Дубль";
		$or1 = "UPDATE income SET order_schet = '$numCash' WHERE order_schet = '$numCash'";
		mysqli_query($db, $or1);
	}
	//если такие записи уже существуют, возвращается 1, и не добавляется в БД
	if(mysqli_num_rows($resUnic) <= 0)
	{
		$queryAdd = "INSERT INTO income (date, order_num, sum, payer_inn, comment, account, order_schet)
		VALUES ('$dateCash', '$orderCash', '$summCash', '$innCash', '$commentCash', '$accountCash', '$numCash')";
		mysqli_query($db, $queryAdd);
	}
}
?>
