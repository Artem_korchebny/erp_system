<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Продажи по продуктам</title>
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
<?php
include "functions-dump.php";// подключаем функцию dump
include "dbconnect.php";// подключаем функцию getConnect с параметрами подключения к БД

$db = getConnect();// подключаемся к БД

$productMas = array();// Объявляем $productMas массивом, для того чтобы ниже объединить суммы с одиноковым источником
$summaMas = array();// Объявляем $summaMas массивом, для того чтобы ниже объединить суммы с одиноковым источником

$query = "SELECT * FROM income ORDER BY date ASC";//извлекаем все записи из БД за отсортированные по дате
$sql = mysqli_query($db, $query);
while ($row = mysqli_fetch_array($sql))
{
	$arOr = array();//обнуляем массив $arOr, чтобы информация которая была в него добавлена при прошлой итерации удалилась
	$arSum[] = $row;//Добавляем в массив все данные за определённый месяц из таблицы income
	$sumRow = trim($row['order_schet']);//добавляем в переменную данные из order_schet

	$schekSum = "SELECT * FROM orders WHERE num = '$sumRow'";//извлекаем все данные из таблицы orders, где num = номеру счёта из income
	$schekOrder =  mysqli_query($db, $schekSum);
	while($oRow = mysqli_fetch_array($schekOrder))
	{
		$arOr[] = $oRow;//добавляем в массив все данные, где num = $sumRow
		$arOr1[] = $oRow['num'];
		$fPay = array_unique($arOr1);//Убираем повторяющиеся значения из массива
	}

	if ($row["date"] != null)//если date в массиве не пустая
	{
		$masAllsummOrders = array();//создаём массив в который будет занасится сумма, консультан, продукт за определённый счёт
		for ($i = 0; $i < count($arSum); $i ++)// перебираем данные за определённый месяц из таблицы income
		{
			$sr = $row['order_schet'];
			$sumP = 0;
			for($j = 0; $j <count($arOr); $j ++)
			{
				$or = $arOr[$j]['num'];
				if($sr == $or)// если номера счетов из таблиц income и orders совпадают, то заносим в переменные данные из orders
				{
					$c = $arOr[$j]['consultant'];
					$p = $arOr[$j]['product'];
					$s = $arOr[$j]['sum'];
					$e = $arOr[$j]['expense'];
					$yan = $arOr[$j]['yandex'];
					$sumP = $sumP + (int)$s + (int)$e + (int)$yan;// $e, $yan прибавляем для того чтобы, в таблице выводились записи (т.к. у нас условие что запись выводится при равной сумме из income и orders)
				}
			}
			if($sumP != 0)// если сумма не равна 0, то
			{
				$masAllsummOrders[$sr] = array("$sumP", $c, $p);//поместили в массив сумму, консультанта, продукт в котором ключь это номер счёта из таблицы income
			}
			$sr = $arSum[$i]['order_schet'];
			foreach ($masAllsummOrders as $key => $value)
			{
				if ($sr == $key)// если номера счетов из таблиц income и массива $masAllsummOrders совпадают
				{
					$t =  $arSum[$i]['sum'];
					if ($t == $value[0])//если суммы по даному счету совпадают (из income и массива $masAllsummOrders)
					{
						for ($p = 0; $p < count($arOr); $p++)
						{
							if ($row["order_schet"] == $arOr[$p]['num'])// если номера счетов из таблиц income и orders совпадают, считаем продукт
							{

								// если стобец "product" не бы заполнен в таблице orders, то берем имя из столбца "appointment"
								if($arOr[$p]['product'] == null)
								{
									$productOrder = $arOr[$p]['appointment'];
								}
								else
								{
									$productOrder = $arOr[$p]['product'];
								}

								
								if ($key == $arOr[$p]['num'])
								{
									$summaMas1 = $arOr[$p]['sum'];//помещаем в переменную $summaMas1 сумму по продукту
								}

								if(in_array("$productOrder", $productMas))// Если в массиве $productMas есть значение $productOrder, то выполняем следующее
								{
									$key1 = array_search("$productOrder", $productMas);// // получаем ключ, в котором такой же продукт
									$obs = $summaMas[$key1] + $summaMas1;// складываем суммы одинаковых продуктов
									$summaMas[$key1] = $obs;// присваиваем существующему продукту сумму одинаковых продуктов
								}
								else// если в массиве такого значения нет, то добавляем значения в массивы
								{
									$productMas[] = $productOrder;
									$summaMas[] = $summaMas1;
								}
							}
						}
					}
				}
			}
		}
	}
}

if (@array_sum($summaMas) > 0 )// если массив $summaMas > 0 , то выводим шапку таблицы
{
	echo "<table id='myTable1'>";
	echo "<thead>";
    echo "<tr>";
   	echo "<th>Продукт</th>";
   	echo "<th>Сумма</th>";
   	echo "</tr>";
	echo "</thead>";
}

for($i = 0; $i < sizeof($productMas); $i ++)// перебираем массивы, извлекая значения и выводя их в таблицу
{
	$productMas2 = $productMas[$i];
    $summaMas2 = $summaMas[$i];
	if ($summaMas2 > 0)// если $summaMas2 < 0 , то ничего выводить не нужно
	{
		echo "<tr>";
			echo "<td class='th'>" . $productMas2 . "</td>";
			echo "<td class='th'>" . $summaMas2 . "</td>";
		echo "</tr>";
	}
}
?>
		</table>
		<p><a href = 'index.php'>На главную</a></p>
	</body>
</html>
