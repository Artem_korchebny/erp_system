<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>КФП</title>
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		<form method='post'>
<?php

include "functions-dump.php";
include "dbconnect.php";
include "salary-dop.php";


/*------ВЫВОД ССЫЛОК С МЕСЯЦАМИ------*/
$db = getConnect();
$query1 = "SELECT month(date), year(date) FROM income";//делаем выборку всех записей из БД
$sql1 = mysqli_query($db, $query1);
while ($monthDate = mysqli_fetch_array($sql1))
{
	$proverka = $monthDate['month(date)'];//переменная для того что бы проверить пусто ли в БД
	$month[] = $monthDate['month(date)'];//добавляем месяца в массив
}
$month12 = array_values(array_unique($month));//В данной переменной массив, со всеми месяцами которые есть в БД

	if (isset($_GET['operation']))//если глобальный массив $_GET["operation"] определён, то
	{
	  $month = $_GET['operation'];//в переменную добавляем значение глобального массива
	}
	else
	{
	  $_GET['operation'] = date('m');// Если в GET ничего нет, то добавляем текущий месяц
	  $month = $_GET['operation'];//текущий месяц
	}
$months = array( 1 => 'Январь' , 'Февраль' , 'Март' , 'Апрель' , 'Май' , 'Июнь' , 'Июль' , 'Август' , 'Сентябрь' , 'Октябрь' , 'Ноябрь' , 'Декабрь' ); // массив с русскими названиями месяцев
for($m = 1;$m <= 12; $m ++)
{
	for($k=0; $k < count($month12); $k++)
	{
		if($month12[$k] == $m)
		{
			$months_date = date($months[date($m)]);// данный блок кода нужен для того, чтобы выбранный месяц выделялся жирным шрифтом
			if ($month != $m)
			{
				$may = "<a href='?operation=" . $m ."'>" . $months_date . "</a>";
				echo $may;
			}
			else
			{
				$may = "<a id='videleno' href='?operation=" . $m ."'>" . $months_date . "</a>";
				echo $may;
			}

		}
	}
}
/*-----/ВЫВОД ССЫЛОК С МЕСЯЦАМИ-------*/

$productMas = array();// Объявляем $productMas массивом, для того чтобы ниже вывести данный в таблицу доход
$summaMas = array();// Объявляем $summaMas массивом, для того чтобы ниже вывести данный в таблицу доход

$queryProductNew = "SELECT * FROM products ORDER BY product";
$sqlProductNew = mysqli_query($db, $queryProductNew);
while ($rowProductNew = mysqli_fetch_array($sqlProductNew))
{
	$ProductNew[] = $rowProductNew;//получаем все продукты
}
$queryWork = "SELECT * FROM complited_work WHERE month(date)= '$month'";
$sqlWork = mysqli_query($db, $queryWork);
while ($rowWork = mysqli_fetch_array($sqlWork))
{
	$work[] = $rowWork["support_time"];//отработанные часы по обслуживанию
}
$queryAdjustments = "SELECT * FROM adjustments WHERE month(date)= '$month' AND variable = 'Премия оператору'";
$sqlAdjustments = mysqli_query($db, $queryAdjustments);
while ($rowAdjustments = mysqli_fetch_array($sqlAdjustments))
{
	$Adjustments[] = $rowAdjustments["value"];//получаем значения для Премия оператору
}
$queryAdjustments1 = "SELECT * FROM adjustments WHERE month(date)= '$month' AND variable = 'Премия Скуматову'";
$sqlAdjustments1 = mysqli_query($db, $queryAdjustments1);
while ($rowAdjustments1 = mysqli_fetch_array($sqlAdjustments1))
{
	$Adjustments1[] = $rowAdjustments1["value"];//получаем значения для Премия Скуматову
}

$Employees_employe = array();// Объявляем $Employees_employe массивом, для того чтобы ниже посчитать актуальные оклады
$Employees_data = array();// Объявляем $Employees_employe массивом, для того чтобы ниже посчитать актуальные оклады
$Employees_salary = array();// Объявляем $Employees_employe массивом, для того чтобы ниже посчитать актуальные оклады

/*----- /КОД ОТВЕЧАЮЩИЙ ЗА ОТОБРАЖЕНИЕ ТАБЛИЦЫ "ДОХОД" + отображения Отработанных часов по обслуживанию -----*/
$queryEmployees = "SELECT * FROM employees_changes WHERE month(date) <= '$month'";//извлекаем все записи из БД
$sqlEmployees = mysqli_query($db, $queryEmployees);
while ($rowEmployees = mysqli_fetch_array($sqlEmployees))
{
	$employe = $rowEmployees["employe"];// помещаем в переменную $employe id сотрудника
	if(in_array("$employe", $Employees_employe))// Если в массиве $Employees_employe есть значение $employe, то выполняем следующее
	{
		$key1 = array_search("$employe", $Employees_employe);//получаем ключ, в котором такой же $employe
		$data1 = strtotime($Employees_data[$key1]);// берем дату из $Employees_data с таким же ключом как и у $employe и приводим ее к виду strtotime
		$data2 = strtotime($rowEmployees["date"]);//извлекаем дату из БД и приводим ее к виду strtotime

		if ($data1 > $data2)// Если дата из $Employees_data актуальнее даты из БД, то ничего не делаем
		{

		}
		else// Если дата из БД актуальнее даты из $Employees_data, то заменяем оклад в $Employees_salary на актуальный
		{
			$Employees_salary[$key1] = $rowEmployees["salary"];
			$Employees_data[$key1] = $rowEmployees["date"];
		}
	}
	else// Если в массиве $Employees_employe нет значение $employe, то добавляем значения в массивы
	{
		$Employees_employe[] = $rowEmployees["employe"];
		$Employees_data[] = $rowEmployees["date"];
		$Employees_salary[] = $rowEmployees["salary"];
	}
}
$Employees = @array_sum($Employees_salary);// считаем актуальные оклады сотрудников

$querySpending = "SELECT * FROM spending WHERE month(date) = '$month'";
$sqlSpending = mysqli_query($db, $querySpending);
while ($rowSpending = mysqli_fetch_array($sqlSpending))
{
	$Spending_item = $rowSpending["item"];
	$Spending_sum = $rowSpending["sum"];
	$spenConsul[] = $rowSpending;//добавляем все начисления по ЗП в массив за определенный месяц
	$queryCosts = "SELECT * FROM costs WHERE constant = 1 AND costs_name = '$Spending_item'";
	$sqlCosts = mysqli_query($db, $queryCosts);
	if(mysqli_num_rows($sqlCosts) > 0)
	{
		$Spending[] = $Spending_sum;
	}
}

$query = "SELECT * FROM income WHERE month(date)= '$month' ORDER BY date ASC";
$sql = mysqli_query($db, $query);
while ($row = mysqli_fetch_array($sql))
{
	$arSum[] = $row;//Добавляем в массив все данные за определённый месяц из таблицы income
	$sumRow = trim($row['order_schet']);//добавляем в переменную данные из order_schet
    $recipient = $row['account'];
    if ($recipient == 40802810818350019540)
    {
        $sumIp[] = $row['sum'];//добавляем в массив сумму оплат по ИП
    }
    elseif ($recipient == 40702810818350002110)
    {
        $sumOOO[] = $row['sum'];//добавляем в массив сумму оплат по ООО
    }
	$schekSum = "SELECT * FROM orders WHERE num = '$sumRow'";//выбираем все данные из таблицы orders, где num = номеру счёта из income
	$schekOrder =  mysqli_query($db, $schekSum);
	while($oRow = mysqli_fetch_array($schekOrder))
	{
		$arOr[] = $oRow;//добавляем в массив все данные, где num = $sumRow
		$arOr1[] = $oRow['num'];
		$fPay = array_unique($arOr1);
		$ya[] = $oRow["yandex"];
	}

	if ($row["date"] != null)//если date в массиве не пустая
	{
		$innlegal = $row["payer_inn"];//добавляем в переменную инн из таблицы income
		$legalQuery = "SELECT * FROM customers WHERE inn = '$innlegal'";//выбираем все записи из таблицы customers, где инн income равен инн customers
		$sqllegal = mysqli_query($db, $legalQuery);
		while ($rowCustomers = mysqli_fetch_array($sqllegal))
		{
			$masAllsummOrders = array();//создаём массив в который будет занасится сумма, консультан, продукт за определённый счёт
			for ($i = 0; $i < count($arSum); $i ++)
			{
				$sr = $row['order_schet'];
				$sumP = 0;
				for($j = 0; $j <count($arOr); $j ++)
				{
					$or = $arOr[$j]['num'];
					if($sr == $or)
					{
						$c = $arOr[$j]['consultant'];
						$p = $arOr[$j]['product'];
						$s = $arOr[$j]['sum'];
						$e = $arOr[$j]['expense'];
						$yan = $arOr[$j]['yandex'];
						$sumP = $sumP + (int)$s + (int)$e + (int)$yan;// $e прибавляем для того чтобы, в таблице выводились записи (т.к. у нас условие что запись выводится при равной сумме из income и orders)
					}
				}
				if($sumP != 0)
				{
					$masAllsummOrders[$sr] = array("$sumP", $c, $p);//поместили в массив сумму, консультанта, продукт в котором ключь это номер счёта из таблицы income
				}
				$sr = $arSum[$i]['order_schet'];
				foreach ($masAllsummOrders as $key => $value)
				{
					if ($sr == $key)
					{
						$t =  $arSum[$i]['sum'];
						if ($t == $value[0])
						{
							for ($p = 0; $p < count($arOr); $p++)
							{
								if ($row["order_schet"] == $arOr[$p]['num'])
								{
										$keyOrder = $key;//берём счет у которого суммы совпали
										$valueNum = $arOr[$p]['num'];//счёт из ордерс
										if ($keyOrder == $valueNum)
										{
											if($arOr[$p]['product'] == null)
											{
												$productOrder = $arOr[$p]['appointment'];//добавляем дефолтный продукт если обновлённый продукт пуст
											}
											else{
												$productOrder = $arOr[$p]['product'];
											}

											$queryProducts = "SELECT product FROM products WHERE product = '$productOrder'";
											$sqlProducts = mysqli_query($db, $queryProducts);
											$appointment = $arOr[$p]['appointment'];

											if ($keyOrder == $valueNum)
											{
												$summaMas1 = $arOr[$p]['sum'];//добавляем в переменную сумму
											}

                      if(preg_match("(Обслуживание сайта)", "$productOrder"))
                      {
                          $summaObsl[] = $summaMas1;//добавляем в массив сумму продукта "Обслуживание сайта"
                      }
                      else
                      {
												if(in_array("$productOrder", $productMas))// Если в массиве $productMas есть значение $productOrder, то выполняем следующее
												{
													$key1 = array_search("$productOrder", $productMas);// // получаем ключ, в котором такой же продукт
													$obs = $summaMas[$key1] + $summaMas1;// складываем суммы одинаковых продуктов
													$summaMas[$key1] = $obs;// присваиваем существующему продукту сумму одинаковых продуктов
												}
												else// если в массиве такого значения нет, то добавляем значения в массивы
												{
													$productMas[] = $productOrder;//добавляем в массив названия продуктов
													$summaMas[] = $summaMas1;//добавляем в массив сумму для каждого продукта
												}
											}
										}
								}
							}
						}
					}
				}
			}
		}
	}
}
$percentAll = array();//объявляем массив в котором будут хранится сумма процентов по каджому продукту
for($p = 0; $p < count($ProductNew); $p++)
{
    if(!preg_match("(Обслуживание сайта)", $ProductNew[$p]['product']))//если в продукте не найдено словосочитание "Обслуживание сайта", то добавляем в массив сумму всех процентов
    {
        $percentAll[$ProductNew[$p]["product"]] =  (int)$ProductNew[$p]["agency_percent"] +  (int)$ProductNew[$p]["manager_percent"]
        + (int)$ProductNew[$p]["chief_percent"] + (int)$ProductNew[$p]["marketing_percent"] + (int)$ProductNew[$p]["specialist_percent"] + (int)$ProductNew[$p]["expense_percent"];
    }
    else{
			//добавляем сумму всех процентов, где в продукте есть "Обслуживание сайта"
        $percentAllS =  (int)$ProductNew[$p]["agency_percent"] +  (int)$ProductNew[$p]["manager_percent"]
        + (int)$ProductNew[$p]["chief_percent"] + (int)$ProductNew[$p]["marketing_percent"] + (int)$ProductNew[$p]["specialist_percent"] + (int)$ProductNew[$p]["expense_percent"];
    }
}
$percentAll['Обслуживание сайта'] = $percentAllS;//добавляем продукт "Обслуживание сайта" и его сумму процентов.
$summaObsl123 = @array_sum($summaObsl);//сумма продукта "Обслуживание сайта"

$productMas[] = "Обслуживание сайта";//добавляем продукт "Обслуживание сайта" ко всем продуктам
$summaMas[] = $summaObsl123;//добавляем сумму к продукту "Обслуживание сайта"

for($w = 0; $w < count($work); $w++)
{
    $hours[] = (int)explode(":", $work[$w])[0] * 60;//отработанные часы переводим в минуты
    $min[] = (int)explode(":", $work[$w])[1];//минуты отработанных часов

}
$time = @array_sum($hours) + @array_sum($min);//сумма минут отработанных часов по обслуживанию
$timeSumInt = intval($time/60);//из всем минут выделяем часы для вывода
$ostatocTime = $time % 60;//выделяем оставшиеся минуты
if ($ostatocTime < 10)
{
    $ostatocTime = "0" . $ostatocTime;//подставляем 0 если минуты меньше 10(например, 12:5 => 12:05)
}
$timeAllSum = $timeSumInt . ":" . $ostatocTime;//собираем часы и минуты для вывода времени
$pokazatel = $time * (400/60);//подсчёт показателя отработанных часов по обслуживанию(сначала узнаём сколько стоит одна минута, потом умножаем на сумма отработанных часов)
@$pokazatel2 = ($pokazatel/$summaObsl123) * 100;//теперь этот показатель делем на объём по "Обслуживанию сайта" и переводим в проценты

$sumIp = @array_sum($sumIp);//сумма оплат по ИП
$sumOOO = @array_sum($sumOOO);//сумма оплат по ООО
//“Отработанные часы по обслуживанию” в скобках вывести: (Эти часы * 400 руб. = получившаяся сумма поделенная на объем по обслуживанию за месяц в процентах)
echo "<p>Отработанные часы по обслуживанию = " . $timeAllSum . " (" . round($pokazatel, 2) . " = " . round($pokazatel2, 1) . "%)" . "</p>";
if (@array_sum($summaMas) > 0 )// если массив $summaMas > 0 , то выводим шапку таблицы
{
	echo "<h3>Доход</h3>";
	echo "<table id='myTable'>";
	echo "<thead>";
    echo "<tr>";
   	echo "<th>Продукт</th>";
   	echo "<th>Сумма</th>";
	echo "<th>%</th>";
    echo "<th>Прибыль</th>";
	echo "<th>VEK</th>";
	echo "<th>SGV</th>";
	echo "<th>KDA</th>";
	echo "<th>VKA</th>";
   	echo "</tr>";
	echo "</thead>";
}
$allsummaMas = @array_sum($summaMas);// считаем общую сумму, всех продуктов(она нужна для вычисления процентов)
foreach ($percentAll as $key => $value)
{
    for($i = 0; $i < count($productMas); $i ++)// перебираем массивы, вытягивая значения и выводя их в таблицу
    {
        if($key == $productMas[$i]){
        	$productMas2 = $productMas[$i];
            $summaMas2 = $summaMas[$i];
        	$procent = $summaMas2*100/$allsummaMas;//формула такая: сумму продукта умножаем на 100% и делим на общую сумму
			$procent2[] = $procent;
			$prib = $summaMas2*((100 - $value - 4)/100); //расчёт прибыли: сумма продукта * на (100% - процент по продукту - 4%)
			$pribil[] = $prib;
			$VEK = 0;//обнуляем переменную, для того чтобы если нет совпадающего продукта в переменной не осталось значение с прошлой итерации
			$SGV = 0;//обнуляем переменную, для того чтобы если нет совпадающего продукта в переменной не осталось значение с прошлой итерации
			$KDA = 0;//обнуляем переменную, для того чтобы если нет совпадающего продукта в переменной не осталось значение с прошлой итерации
			$VKA = 0;//обнуляем переменную, для того чтобы если нет совпадающего продукта в переменной не осталось значение с прошлой итерации

			$queryDividends = "SELECT * FROM dividends WHERE product = '$productMas2'";// выбираем все данные из таблицы dividends где совпадают продукты
			$sqlDividends = mysqli_query($db, $queryDividends);
			while ($rowDividends = mysqli_fetch_array($sqlDividends))
			{
				$VEK = $rowDividends["VEK"]/100;// получаем процент $VEK
				$SGV = $rowDividends["SGV"]/100;// получаем процент $SGV
				$KDA = $rowDividends["KDA"]/100;// получаем процент $KDA
				$VKA = $rowDividends["VKA"]/100;// получаем процент $VKA
			}
        	if ($summaMas2 > 0)// если $summaMas2 < 0 , то ничего выводить не нужно
        	{
				$pribPercent = $prib/$allsummaMas*100;//
        		echo "<tr>";
                echo "<td class='th'>" . $productMas2 . "</td>";
                echo "<td class='th'>" . $summaMas2 . "</td>";
        		echo "<td class='th'>" . round($procent, 2) . "</td>";//round($procent, 2) - это для того чтобы после запятой было не больше 2 символов
                echo "<td class='th'>" . $prib . " (" . round($pribPercent, 1) . "%)</td>";
				echo "<td class='th'>" . $prib * $VEK. "</td>";
				echo "<td class='th'>" . $prib * $SGV. "</td>";
				echo "<td class='th'>" . $prib * $KDA. "</td>";
				echo "<td class='th'>" . $prib * $VKA. "</td>";
        		echo "</tr>";

				$VEK1[] = $prib * $VEK;//добавляем значения в массив, для подсчета общей суммы в строке Итого
				$SGV1[] = $prib * $SGV;//добавляем значения в массив, для подсчета общей суммы в строке Итого
				$KDA1[] = $prib * $KDA;//добавляем значения в массив, для подсчета общей суммы в строке Итого
				$VKA1[] = $prib * $VKA;//добавляем значения в массив, для подсчета общей суммы в строке Итого
        	}
        }
    }
}
/*----- /КОД ОТВЕЧАЮЩИЙ ЗА ОТОБРАЖЕНИЕ ТАБЛИЦЫ "ДОХОД" + отображения Отработанных часов по обслуживанию -----*/
$pribil = @array_sum($pribil);//сумма прибыли
$procent2 = @array_sum($procent2);
$Adjustments = @array_sum($Adjustments);
$Adjustments1 = @array_sum($Adjustments1);
$Spending1 = @array_sum($Spending);
$VEK2 = @array_sum($VEK1);//общая сумма $VEK
$SGV2 = @array_sum($SGV1);//общая сумма $SGV
$KDA2 = @array_sum($KDA1);//общая сумма $KDA
$VKA2 = @array_sum($VKA1);//общая сумма $VKA
echo "<thead>";
echo "<tr>";
echo "<td class='th'>Итого:</td>";
echo "<td class='th'>" . $allsummaMas . "</td>";
echo "<td class='th'>" . round($procent2, 2) . " % </td>";
echo "<td class='th'>" . $pribil . "</td>";
echo "<td class='th'>" . $VEK2 . "</td>";
echo "<td class='th'>" . $SGV2 . "</td>";
echo "<td class='th'>" . $KDA2 . "</td>";
echo "<td class='th'>" . $VKA2 . "</td>";
echo "</tr>";
echo "</thead>";
echo "</table><br>";

$sala = getSalary();

$premiaruk = $allsummaMas * 4/100;
$nalogDohod = ($sumIp + $sumOOO - array_sum($ya)) * 0.06;//“Налог с доходов” - сумма оплат по ИП, умноженная на 6%.
$kommisaBanka = ($sumIp + $sumOOO) * 0.015;//“Налог с доходов” - сумма оплат по ИП, умноженная на 1,5%.
$pribilProduct = $pribil - $nalogDohod - $kommisaBanka;//“Прибыль с продуктов” = Сумма по столбцу Прибыль - Налог с доходов ИП - Комиссия банка.
$pribkomp = $pribilProduct - $premia_obshay2 - $Adjustments - $Adjustments1 - $Employees1 - $premiaruk - $Spending1;//"Прибыль компании" = Прибыль с продуктов - Бонусы консультантам - Бонус оператор - Бонус Скуматов - Оклады - Премия руководителям - Постоянные расходы
$sredrentab = $pribilProduct/$allsummaMas * 100;//Средняя рентабельность продуктов”: Итого по столбцу Прибыль деленная на итого по столбцу Сумма * 100 (в процентах).
$tochbez = ($Employees1 + $premia_obshay2 + $Adjustments + $Adjustments1 + $premiaruk + $Spending1)/$sredrentab;//“Точка безубыточности”: (Оклады + Бонусы консультантам + Бонус оператору + Бонус Скуматову + Бонус руководителям + Постоянные расходы) / Средняя рентабельность продуктов
$rentkompany = $pribkomp/$allsummaMas * 100;//“Рентабельность компании”: Прибыль компании / Сумму продаж * 100%



echo "<p>Налог с доходов (6%) = " . $nalogDohod . "</p>";
echo "<p>Комиссия банка (1.5%) = " . $kommisaBanka . "</p>";
echo "<p>Прибыль с продуктов = " . $pribilProduct . "</p>";
echo "<p>Премия оператору = " . $Adjustments . "</p>";
echo "<p>Премия Скуматову = " . $Adjustments1 . "</p>";
echo "<p>Бонусы консультантам = " . $premia_obshay2 . "</p>";
echo "<p>Оклады = " . $Employees . "</p>";
echo "<p>Премия руководителям = " . $premiaruk . "</p>";
echo "<p>Постоянные расходы = " . $Spending1 . "</p>";
echo "<p>Прибыль компании = " . $pribkomp . "</p>";
echo "<p>Средняя рентабельность продуктов = " . round($sredrentab, 1) . "%</p>";
echo "<p>Точка безубыточности = " . round($tochbez, 2)*100 . "</p>";
echo "<p>Рентабельность компании = " . round($rentkompany, 1) . "%</p>";
echo "<p><a href = 'index.php'>На главную</a></p>";
?>
</form>
</body>
</html>
