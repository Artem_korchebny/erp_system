<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<title>Пополнение источников</title>
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link type="text/css" rel="stylesheet" href="css/style1.css">
		<script type="text/javascript" src="js/jquery-latest.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
<?
include "dbconnect.php";// подключаем функцию getConnect с параметрами подключения к БД

$db = getConnect();// подключаемся к БД

$querySource = "SELECT * FROM sources ORDER BY name ASC";//извлекаем все записи из БД отсортированных по полю name
$sqlSource = mysqli_query($db, $querySource);
while ($rowSource = mysqli_fetch_array($sqlSource))
{
	$source[] = $rowSource;//извлекаем все записи из БД отсортированных по полю name и помещаем в массив $source
}
?>
		<form method="post">
			<p>Дата:<br>
				<input name="date" value="<?echo date('Y-m-d')?>" type="date">
			</p>

			<p>Источник:<br>
				<select name="source">
					<option></option>
						<?
						for ($i = 0; $i < count($source); $i ++)
						{
							$source1 = $source[$i]['name'];
							echo "<option>" . $source1 . "</option>";
						}
						?>
				</select>
			</p>

			<p>Сумма:<br>
				<input name="summ" type="text">
			</p>

			<p>Комментарий:<br>
				<input name="comment" type="text">
			</p>

			<input type="submit" name = "go"></input>
		</form>
		<br>
		<a href = 'index.php'>На главную</a>
	</body>
</html>
<?
// добавляем в переменные данные из полученные из форм
$adjustDate = $_POST['date'];
$adjustSource = $_POST['source'];
$adjustSumm = $_POST['summ'];
$adjustComment = $_POST['comment'];

// если нажата кнопка "Отправить" и все поля формы заполнены, то данные с полей формы записываются в БД
if(isset($_POST['go']) && !empty($adjustSource) && !empty($adjustSumm) && !empty($adjustComment) && !empty($adjustDate))
{
    $queryAdjust = "INSERT INTO sources_adjunctions (date, source, sum, comment)
                      VALUES ('$adjustDate', '$adjustSource', '$adjustSumm', '$adjustComment')";
    mysqli_query($db, $queryAdjust);

	//поле записи данных в БД, страница перезагружается и готова к вводу новых данных
    echo "<script type='text/javascript'>window.location = 'sources_adjunctions.php'</script>";
}
?>
